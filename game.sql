-- MySQL dump 10.13  Distrib 5.6.23, for Win64 (x86_64)
--
-- Host: localhost    Database: game
-- ------------------------------------------------------
-- Server version	5.7.7-rc-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `army_cards`
--

DROP TABLE IF EXISTS `army_cards`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `army_cards` (
  `cardId` int(11) NOT NULL,
  `literal` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `armyId` int(11) NOT NULL,
  `position` int(11) NOT NULL,
  `damage` int(11) NOT NULL,
  `currentHP` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `army_cards`
--

LOCK TABLES `army_cards` WRITE;
/*!40000 ALTER TABLE `army_cards` DISABLE KEYS */;
/*!40000 ALTER TABLE `army_cards` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `battles`
--

DROP TABLE IF EXISTS `battles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `battles` (
  `battleid` int(11) NOT NULL,
  `status` int(11) DEFAULT '0',
  `inviterId` int(11) DEFAULT NULL,
  `guestId` int(11) DEFAULT NULL,
  `forwardId` int(11) DEFAULT NULL,
  `defenderId` int(11) DEFAULT NULL,
  `justActionResult` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`battleid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `battles`
--

LOCK TABLES `battles` WRITE;
/*!40000 ALTER TABLE `battles` DISABLE KEYS */;
/*!40000 ALTER TABLE `battles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `deck_cards`
--

DROP TABLE IF EXISTS `deck_cards`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `deck_cards` (
  `deckId` int(11) NOT NULL,
  `number` int(11) NOT NULL,
  `literal` varchar(20) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `deck_cards`
--

LOCK TABLES `deck_cards` WRITE;
/*!40000 ALTER TABLE `deck_cards` DISABLE KEYS */;
/*!40000 ALTER TABLE `deck_cards` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `heroes`
--

DROP TABLE IF EXISTS `heroes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `heroes` (
  `heroId` int(11) NOT NULL,
  `userId` mediumtext,
  `name` varchar(30) NOT NULL,
  `photo` varchar(20) NOT NULL,
  `currentHP` int(11) NOT NULL,
  `heroClass` varchar(20) DEFAULT NULL,
  `level` int(11) NOT NULL,
  `exp` int(11) NOT NULL,
  `needExp` int(11) NOT NULL,
  `cash` int(11) NOT NULL,
  `deckId` int(11) DEFAULT NULL,
  `powersId` int(11) DEFAULT NULL,
  `armyId` int(11) DEFAULT NULL,
  PRIMARY KEY (`heroId`)
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `heroes`
--

LOCK TABLES `heroes` WRITE;
/*!40000 ALTER TABLE `heroes` DISABLE KEYS */;
INSERT INTO `heroes` VALUES (100000,'1','gabriel','face17.jpg',50,'Архимаг',22,28,74,1948,NULL,NULL,NULL),(100001,'2','LOL','face8.jpg',50,'Архимаг',1,0,10,0,NULL,NULL,NULL),(100002,NULL,'solmyr','face5.jpg',20,'Воин',1,5,10,1,NULL,NULL,NULL),(100003,NULL,'el-classico','face8.jpg',20,'Варвар',11,21,25,527,NULL,NULL,NULL),(100004,NULL,'kasabian','face19.jpg',20,'Маг',1,8,10,527,NULL,NULL,NULL),(100005,NULL,'Призрак','face6.jpg',20,'Архимаг',1,0,10,27,NULL,NULL,NULL),(100006,NULL,'Чебурашка в апельсинах','face6.jpg',20,'Архимаг',1,0,10,62,NULL,NULL,NULL),(362696,'12','445334','face19.jpg',20,'null',1,0,10,0,NULL,NULL,NULL),(559882,'11','lol3','face19.jpg',20,'Целитель',1,0,10,0,NULL,NULL,NULL),(589621,'14','qwerty','face19.jpg',20,'Целитель',1,0,10,0,NULL,NULL,NULL),(619235,'13','44242344232','face19.jpg',20,'Джигурда',1,0,10,0,NULL,NULL,NULL),(788099,'8','heroName','face8.jpg',20,'Архимаг',1,0,10,0,NULL,NULL,NULL),(881308,'15','wwwwwwwwww','face19.jpg',20,'Чародей',1,0,10,0,NULL,NULL,NULL),(896767,'10','lol2','face19.jpg',20,'Джигурда',1,0,10,0,NULL,NULL,NULL),(918387,'9','User2','face8.jpg',20,'Архимаг',1,0,10,0,NULL,NULL,NULL),(999999,'765657','Архимаг','face15.jpg',10,'Архимаг',1,0,10,0,NULL,NULL,NULL);
/*!40000 ALTER TABLE `heroes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `powers`
--

DROP TABLE IF EXISTS `powers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `powers` (
  `powersId` int(11) NOT NULL,
  `element` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `current` int(11) NOT NULL,
  `growth` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `powers`
--

LOCK TABLES `powers` WRITE;
/*!40000 ALTER TABLE `powers` DISABLE KEYS */;
/*!40000 ALTER TABLE `powers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(12) COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `username_2` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'gabriel.exe@yandex.ru','123456','Gabriel.exe'),(2,'lol@yandex.ru','123456','Lol'),(3,'34234322@mail.ru','123456','4234243232'),(4,'123@ma33333','123456','3333333'),(5,'12343@mail.ru','123456','33343433333'),(6,'1243343@mail.ru','123456','444333'),(7,'12434343343@mail.ru','123456','4443335555'),(8,'1422423@yandex.ru','123456','432432'),(9,'user@yanddex.ru','123456','User2'),(10,'55776@yandex.ru','123456','3543454'),(11,'787879878@yandex.ru','123456','878878978'),(12,'35345@yandex.ru','123456','53445435'),(13,'53453443543@yandex.ru','123456','5354535'),(14,'53543543543@yandex.ru','123456','55433533'),(15,'534545534@yandex.ru','123456','767687687');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-10-27  7:47:41

(function($){

	requirejs.config({
		"baseUrl" : "/static/js/source/game",
		"shim" : {
			"libs/underscore-min" : {
				exports: "_"
			}
		}
	});

	Game = {
		"widgets" : {
			"tabs" : {}
		},
		"cards" : {}
	};



	require(
		[
			"controllers/Common"
		],
		function(){
			if($(".page-battle").length > 0){
				require(
					[
						"controllers/pages/Battlefield"
					],
					function(){}
				)
			}

			if($(".page-start").length > 0){
				require(["controllers/pages/Start"]);
			}
		}
	);


})(jQuery);

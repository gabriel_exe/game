(function($){

    define(
        "controllers/pages/Battlefield",
        [
			"widgets/battle/Army",
            "widgets/battle/BattleAction",
            "widgets/battle/Card",
			"widgets/battle/Deck",
            "widgets/battle/Hero",
			"widgets/battle/Powers"
        ],
        function(Army, BattleAction, Card, Deck, Hero, Powers){

            Game.BattleAction = new BattleAction();

			Game["EnemyArmy"] = new Army({
				"element": $(".army.enemy-army")
			});

			Game["MyArmy"] = new Army({
				"element": $(".army.my-army")
			});

			Game["MyDeck"] = new Deck({
				"element": $(".deck.my")
			});

			Game["EnemyDeck"] = new Deck({
				"element": $(".deck.enemy")
			});

            $(".battlefield .card").each(function(index, element) {

                Game.cards["card_" + $(element).data("card-id")] = new Card({
                    "element": $(element)
                });
            });

            $(".heroes .hero").each(function(index, element){
                Game[$(element).attr("id")] = new Hero({"element" : $(element)});
            });

			$(".powers").each(function(index, element){
				Game["powers_" + $(this).data("powers-id")] = new Powers({"element" : $(element)});
			});
        }
    )
})(jQuery);
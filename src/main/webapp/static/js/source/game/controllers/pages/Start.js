(function($){
	define(
		"controllers/pages/Start",
		[
			"widgets/Gallery",
			"widgets/Tabs"
		],
		function(Gallery, Tabs){

			Game.widgets.tabs["TabsStartForms"] = new Tabs({
				"element" : $("#tabs-start-forms")
			});


			var galleryHeroPhotos = $("#gallery-hero-photos");
			Game.widgets["HeroPhotosGallery"] = new Gallery({
				"element": galleryHeroPhotos,
				"galleryMainImage" : $(".main-image img", galleryHeroPhotos),
				"galleryElementsList" : $(".gallery-list", galleryHeroPhotos)
			});




		}
	)
})(jQuery);
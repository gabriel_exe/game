(function($){
	define(
		"widgets/battle/Army",
		[
			"widgets/battle/Card"
		],
		function(Card){

			function Army(options){
				this.init(options)
			}

			Army.prototype = {

				"constructor" : "Army",

				"init" : function(options){

					var defaults = {
							"element" : null,
							"cards" : {}
						},
						namespace = this;

					$.extend(true, this, defaults, options);

					if(this.element == null) return false;

					namespace.element.find(".card").each(function(index, element){
						namespace.cards[$(element).data("card-id")] = new Card({
							"element": $(element)
						});
					});
				},

				"activateSlots" : function(intendedFor){

					var namespace = this,
						slots;

					switch (intendedFor){
						case "setCard" : slots = $(namespace.element).find(".slot:empty"); break;
						case "castSpell" : slots = $(namespace.element).find(".slot:parent"); break;
					}

					slots.each(function(index, element){
						$(element)
							.addClass("active")
							.click(function(){
								Game.BattleAction.target = $(this).parent().index();
								Game.BattleAction.send();
								Game.MyArmy.deactivateSlots();
								Game.EnemyArmy.deactivateSlots();
								Game.MyDeck.deactivateAllCards();
								Game.MyDeck.removeEvents();
							});
					});

					return this;
				},

				"deactivateSlots" : function(){
					var namespace = this;

					$(namespace.element)
						.find(".slot.active")
						.off()
						.removeClass("active");

					return this;
				}
			};

			return Army;
		}
	)
})(jQuery);
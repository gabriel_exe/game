(function($){
	define(
		"widgets/battle/Powers",
		[

		],
		function(){

			function Powers(options){
				this.init(options);
			}

			Powers.prototype = {

				"constructor" : "Powers",

				"init" : function(options){

					var defaults = {
							"element" : null
						},
						namespace = this;

					$.extend(true, this, defaults, options);

					if(this.element == null){
						return false;
					}
				},

				"changePowers" : function(powers, animate){

					var namespace = this;

					for(var key in powers){
						if(powers.hasOwnProperty(key)){
							var power = powers[key][0],
								element = power["element"][0],
								type = power["type"][0],
								count = power["count"][0],
								newValue = power["newValue"][0],
								updateElementalDamage = power["updateElementalDamage"][0],
								el = namespace.element.find(".power." + element + " ." + type + " .value");

							el.text(newValue);

							if(animate){
								var color = (count < 0) ? "#ff0000" : "#00ff00";

								el.animate({
									"color" : color
								}, 500, "linear", function(){
									$(this).animate({
										"color" : "#9E1B1B"
									}, 500, "linear", false);
								});
							}

							if(_.size(updateElementalDamage) > 0){
								for(key in updateElementalDamage){
									if(updateElementalDamage.hasOwnProperty(key)){
										var damage = updateElementalDamage[key][0];
										Game.cards["card_" + key].changeDamage(damage);
										console.log("updateElementalDamage", key, damage);
									}
								}
							}
						}
					}
				}
			};

			return Powers;
		}
	)
})(jQuery);
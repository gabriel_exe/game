;(function($){

    define(
        "widgets/Gallery",
        [
            //
        ],
        function(){

            function Gallery(options){
                this.init(options)
            }

            Gallery.prototype = {
                "constructor"       :   Gallery,
                "init"              :   function( options ) {

                    var defaults = {
							"element" : undefined,
							"galleryMainImage" : null,
							"galleryElementsList" : null,
							"galleryActiveElement" : 0,
							"navButtons" : true
                        },
                        namespace = this;

                    $.extend(true, this, defaults, options);

					if(this.element.hasClass("gallery-initialized")){
						return;
					}

					this.galleryMainImageWrapper = $(this.galleryMainImage).parent();
					this.galleryItems = this.galleryElementsList.find("li");
					this.activeItem = this.galleryItems.filter(".active");

					namespace.changePhoto(this.galleryItems.eq(this.galleryActiveElement));

					this.galleryItems.on("click", function(){
						namespace.changePhoto(this);
					});

					if(this.navButtons &&  this.galleryItems.length > 1){
						namespace.addNavButtons();
					}

					this.element.addClass("gallery-initialized");
                },

				"addNavButtons" : function(){
					var namespace = this;

					var prevButton = $("<div/>", {"class" : "nav nav-prev user-select"}).on("click", function(e){
							namespace.changePhoto(namespace.detectChangedPhoto(e))
						}),
						nextButton = $("<div/>", {"class" : "nav nav-next user-select"}).on("click", function(e){
							namespace.changePhoto(namespace.detectChangedPhoto(e))
						});

					this.galleryMainImageWrapper.append(prevButton, nextButton);
				},

				"detectChangedPhoto" : function(e){

					var changedElement;

					if($(e.target).hasClass("nav-next")){
						changedElement = (this.activeItem.next().length == 0)
							? this.galleryItems.filter(":first-child")
							: this.activeItem.next();
					} else{
						changedElement = (this.activeItem.prev().length == 0)
							? this.galleryItems.filter(":last-child")
							: this.activeItem.prev();
					}

					return changedElement;
				},

				"changePhoto" : function(element){

					var src = $(element).find("img").attr("src");
					if($(element).find("img").data("scaled-image-src") != "undefined"){

						var scaledImageSrc = $(element).find("img").data("scaled-image-src");
						this.galleryMainImage.attr({"data-scaled-image-src" : scaledImageSrc});
					}
                    this.galleryMainImage.attr({"src" : src}).parent().trigger("image-changed");




					this.activeItem.removeClass("active");
					$(element).addClass("active");
					this.activeItem = $(element);
				},

				"destroy" : function(){
					this.galleryMainImageWrapper.find(".nav")
						.off()
						.remove();

					this.activeItem.removeClass("active");
					this.element.removeClass("gallery-initialized");
				}
            };

            return Gallery;
        }
    )

})(jQuery);

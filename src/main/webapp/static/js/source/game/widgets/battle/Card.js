(function($){
    define(
        "widgets/battle/Card",
        [],
        function(){

            function Card(options){
                this.init(options)
            }

            Card.prototype = {

                "constructor" : "Card",

                "init" : function(options){

                    var defaults = {
                            "element" : null,
                            "isMovedToArmy" : false
                        },
                        namespace = this;

                    $.extend(true, this, defaults, options);

					namespace.element.hover(
						function(){
							var text = namespace.element.find(".description").text();
							$("#card-description").text(text).removeClass("hidden");
						},

						function(){
							$("#card-description").text("").addClass("hidden");
						}
					)

                },

                "isDeckCard" : function(){
                    var namespace = this;
                    return namespace.element.closest(".deck").length > 0;
                },

                "isArmyCard" : function(){
                    var namespace = this;
                    return namespace.element.closest(".army").length > 0;
                },

                "moveToArmy" : function(armyId, target, cardId, whenCalling){
                    var namespace = this,
                        card = this.element.parent().clone(),
						slot = $("[data-army-id=" + armyId +"] .slot").eq(target),
                        startLeftPosition = namespace.element.offset().left,
                        startTopPosition = namespace.element.offset().top,
                        stopLeftPosition = slot.offset().left,
                        stopTopPosition = slot.offset().top;

                    card
                        .appendTo($(".site-wrapper"))
                        .css({
                            "position" : "absolute",
                            "left" : startLeftPosition,
                            "top" : startTopPosition,
							"zIndex" : 10
                        })
                        .animate({
                            "left" : stopLeftPosition,
                            "top" : stopTopPosition
                        }, 1000, "linear", function(){

                            $(this)
								.find(".card")
									.attr({"data-card-id" : cardId})
									.removeClass("disabled")
								.end()
									.css({
										"position" : "static",
										"left" : 0,
										"top" : 0,
										"zIndex" : "auto"
									})
									.appendTo(slot);

							Game.cards["card_" + cardId] = new Card({
								"element" : $(this).find(".card")
							});


							Game.cards["card_" + cardId].whenCalling(whenCalling);


                        });

					return this;

                },

				"whenCalling" : function(whenCalling){

					if(_.size(whenCalling["increaseDamage"][0]) != 0){
						var increaseDamage = whenCalling["increaseDamage"][0];

						for(var key in increaseDamage){
							if(increaseDamage.hasOwnProperty(key)){
								Game.cards["card_" + key].changeDamage(increaseDamage[key][0]);
							}
						}
					}

					if(_.size(whenCalling["changePowers"][0]) != 0){
						var changePowers = whenCalling["changePowers"][0],
							powersId = changePowers["powersId"][0],
							powers = changePowers["powers"][0];

						Game["powers_" + powersId].changePowers(powers, true);
					}

					if(_.size(whenCalling["changeHP"][0]) != 0){
						var changeHP = whenCalling["changeHP"][0];
						for(var target in changeHP){
							if(changeHP.hasOwnProperty(target)){
								var obj = changeHP[target][0],
									t = obj["target"][0],
									count = obj["count"][0],
									newHP = obj["newHP"][0],
									result = obj["result"][0];

								switch (t.substr(0,4)){
									case "card" : Game.cards[t].displayChangeHP(count, newHP, result); break;
									case "hero" : Game[t].displayChangeHP(count, newHP); break;
								}
							}
						}
					}

					if(_.size(whenCalling["additionalSummon"][0]) != 0){
						var additionalSummon = whenCalling["additionalSummon"][0],
							cards = additionalSummon["cards"][0];

						for(var cardKey in cards){
							if(cards.hasOwnProperty(cardKey)){
								var card = cards[cardKey][0]["card"][0],
									heroId = card["heroId"][0];

								Game["hero_" + heroId].summonWithoutAnimation(card);
							}
						}
					}
				},

                "attack" : function(cardAttackResult){

                    var namespace = this,
						targets = cardAttackResult[0],
						vector = (namespace.element.closest(".army.my-army").length > 0) ? "-" : "";

					for(var target in targets){

						if(targets.hasOwnProperty(target)){

							var obj = targets[target][0],
								t = obj["target"][0],
								count = obj["count"][0],
								newHP = obj["newHP"][0],
								result = obj["result"][0];
							
							switch (t.substr(0,4)){
								case "card" : Game.cards[t].displayChangeHP(count, newHP, result); break;
								case "hero" : Game[t].displayChangeHP(count, newHP); break;
							}
						}
					}

                    namespace.element
						.css({"position": "relative", "zIndex" : "10"})
						.animate({
                        "marginTop" : vector + "45px"
                    }, 750, "linear", function() {
                        $(this).animate({
                            "marginTop": "0px"
                        }, 500, "linear", function(){
							$(this).css({"zIndex" : "auto"})
						});
                    });
                },

				"displayChangeHP" : function(count, newHP, result){
					var namespace = this,
						cssClass = (count > 0) ? "plus" : "minus",
						cssColor = (cssClass == "plus")
							? {"color" : "#00ff00"}
							: {"color" : "#ff0000"},
						cssProperties1 = {"top" : "-60px"},
						cssProperties2 = {"top" : "-100px", "opacity" : 0},
						countHP = $("<div/>", {"class" : "change-hp " + cssClass})
							.text((cssClass == "plus") ? "+" + count : count),
						currentHP = namespace.element.find(".hp");

					currentHP
						.text(newHP)
						.animate(cssColor, 500, "linear", function(){
							$(this).animate({"color" : "#000000"}, 500, "linear",false);
						});

					if(count != 0){
						countHP
							.appendTo(currentHP)
							.animate(cssProperties1, 750, "linear", function(){

								if(result != "survival"){
									namespace.death(result);
								}

								$(this).animate(cssProperties2, 500, "linear", function(){
									$(this).remove();
								})
							});
					}

					return this;
				},

				"death" : function(result){
					var namespace = this;

					if(_.size(result["increaseDamage"][0]) != 0){
						var increaseDamage = result["increaseDamage"][0];
						for(var key in increaseDamage){
							if(increaseDamage.hasOwnProperty(key)){
								Game.cards["card_" + key].changeDamage(increaseDamage[key][0]);
							}
						}
					}

					if(_.size(result["changePowers"][0]) != 0){
						var changePowers = result["changePowers"][0],
							powersId = changePowers["powersId"][0],
							powers = changePowers["powers"][0];
						Game["powers_" + powersId].changePowers(powers, false);
					}

					namespace.element.
						animate({
							"opacity" : 0
						}, 500, "linear", function(){

							delete Game.cards["card_" + $(this).data("card-id")];
							$(this).parent().remove();

							if(result["resurrection"] != undefined){

								var resurrection = result["resurrection"][0],
									heroId = resurrection["heroId"][0];

								Game["hero_" + heroId].summonWithoutAnimation(resurrection);
							}

						});

					return this;
				},

				"accent" : function(){
					var namespace = this;

					namespace.element.animate({
						"left" : "-7px",
						"top" : "-7px",
						"width" : "102px",
						"height" : "108px"
					}, 500, "linear", function(){
						$(this).animate({
							"left" : "0",
							"top" : "0",
							"width" : "88px",
							"height" : "94px"
						}, 500, "linear", false);
					});

					namespace.element.find("img").animate({
						"width" : "94px",
						"height" : "94px"
					}, 500, "linear", function(){
						$(this).animate({
							"width" : "80px",
							"height" : "80px"
						}, 500, "linear", false);
					});
				},

				"changeDamage" : function(damage){
					var namespace = this;
					namespace.element.find(".prop.damage").text(damage);
				}
            };

            return Card;
        }
    )
})(jQuery);
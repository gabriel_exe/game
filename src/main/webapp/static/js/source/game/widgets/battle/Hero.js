(function($){
    define(
        "widgets/battle/Hero",
        [
			"widgets/battle/Army",
			"widgets/battle/Card"
		],
        function(Army, Card){

            function Hero(options){
                this.init(options);
            }

            Hero.prototype = {

                "constructor" : "Hero",

                "init" : function(options){

                    var defaults = {
                            "element" : null,
							"army" : null
						},
						namespace = this;

                    $.extend(true, this, defaults, options);

					var armyId = namespace.element.data("army-id");
					namespace.army = new Army({
						"element" : $(".army[data-army-id=" + armyId + "]")
					});
                },



                "displayChangeHP" : function(count, newHP){

                    var namespace = this,
                        cssClass = (count > 0) ? "plus" : "minus",
                        vector = (namespace.element.hasClass("enemy"))
                            ? ""
                            : "-",
                        cssColor = (cssClass == "plus")
                            ? {"color" : "#00ff00"}
                            : {"color" : "#ff0000"},
                        cssProperties1 = {"top" : (vector + "60") + "px"},
                        cssProperties2 = {"top" : (vector + "100") + "px", "opacity" : 0},
                        countHP = $("<div/>", {"class" : "change-hp " + cssClass})
                            .text((cssClass == "plus") ? "+" + count : count),
                        currentHP = namespace.element.find(".hp");

                    currentHP
                        .text(newHP)
                        .animate(cssColor, 500, "linear", function(){
                            $(this).animate({"color" : "#ffffff"}, 500, "linear",false);
                        });

                    countHP
                        .appendTo(currentHP)
                        .animate(cssProperties1, 750, "linear", function(){
                            $(this).animate(cssProperties2, 500, "linear", function(){
                                $(this).remove();
                            })
                        });

                    return this;
                },

				"surrender" : function(heroAction){

					var winner = heroAction["actionResult"][0]["winner"][0],
						looser = heroAction["actionResult"][0]["looser"][0],
						text = looser + " сдался, " + winner + " одерживает победу!\n\r" +
							"Для того чтобы начать новую битву, заново пригласите соперника";

					alert(text);
				},

				"setCard" : function(heroAction){
					var namespace = this,
						heroId = heroAction["heroId"][0],
						card = heroAction["card"][0],
						cardId = heroAction["card"][0]["cardId"][0],
						literal = heroAction["literal"][0],
						armyId = heroAction["armyId"][0],
						target = heroAction["target"][0],
						dispel = heroAction["dispel"][0],
						powersId = dispel["powersId"][0],
						powers = dispel["powers"][0],
						whenCalling = heroAction["whenCalling"][0];

					Game["powers_" + powersId].changePowers(powers, false);

					Game.BattleAction.heroActionDelay += 2000;

					if($(".hero.my").attr("id") == heroId){
						Game.MyDeck.cards[literal].moveToArmy(armyId, target, cardId, whenCalling);
					} else {
						var el = namespace.createCard(card),
							hero = $("#" + heroId);
						el.appendTo(hero);
						Game.EnemyDeck.cards[literal] = new Card({
							"element" : hero.find(".card")
						});
						Game.EnemyDeck.cards[literal].moveToArmy(armyId, target, cardId, whenCalling);
						el.remove();
					}
				},

				"createCard" : function(obj){
					var namespace = this,
						id = obj["cardId"][0],
						photo = obj["photo"][0],
						damage = obj["damage"][0],
						cost = obj["cost"][0],
						hp = obj["hp"][0],
						description = obj["description"][0];

					return $("<div/>", {"class" : "card-wrapper"})
						.append($("<div/>", {"class" : "card card-creature", "data-card-id" : id})
							.append(
								$("<img/>", {"src" : "/static/images/cards/creatures/" + photo}),
								$("<div/>", {"class" : "prop cost"}).text(cost),
								$("<div/>", {"class" : "prop damage"}).text(damage),
								$("<div/>", {"class" : "prop hp"}).text(hp),
								$("<div/>", {"class" : "description hidden"}).text(description)
							)
						);
				},

				"castSpell" : function(heroAction){
					var namespace = this,
						literal = heroAction["literal"][0],
						spellResult = heroAction["spellResult"][0],
						dispel = heroAction["dispel"][0],
						powersId = dispel["powersId"][0],
						powers = dispel["powers"][0];

					Game.BattleAction.heroActionDelay += 1000;

					switch (literal){
						case "FlameWave" : namespace.spellFlameWave(spellResult); break;
						case "Inferno" : namespace.spellInferno(spellResult); break;
						case "Armageddon" : namespace.spellArmageddon(spellResult); break;
						case "Meditation" : namespace.spellMediation(spellResult); break;
						case "AcidRain" : namespace.spellAcidRain(spellResult); break;
						case "CallThunder" : namespace.spellCallThunder(spellResult); break;
						case "Lightning" : namespace.spellLightning(spellResult); break;
						case "ChainLightning" : namespace.spellChainLightning(spellResult); break;
						case "Tornado" : namespace.spellTornado(spellResult); break;
						case "NatureRitual" : namespace.spellNatureRitual(spellResult); break;
						case "NaturalFury" : namespace.spellNaturalFury(spellResult); break;
						case "Revival" : namespace.spellRevival(spellResult); break;
						case "StoneRain" : namespace.spellStoneRain(spellResult); break;
					}

					Game["powers_" + powersId].changePowers(powers, false);
				},

				"summonWithoutAnimation" : function(cardInfo){

					var heroId = cardInfo["heroId"][0],
						armyId = cardInfo["armyId"][0],
						slot = cardInfo["slot"][0],
						cardId = cardInfo["cardId"][0],
						place = $(".army[data-army-id=" + armyId + "] .slot").eq(slot);

					Game["hero_" + heroId].createCard(cardInfo).appendTo(place);
					Game.cards["card_" + cardId] = new Card({"element" : place.find(".card")});
					Game.cards["card_" + cardId].accent()

				},

				"spellFlameWave" : function(spellResult){

					for(var target in spellResult){

						if(spellResult.hasOwnProperty(target)){

							var obj = spellResult[target][0],
								t = obj["target"][0],
								count = obj["count"][0],
								newHP = obj["newHP"][0],
								result = obj["result"][0];

							switch (t.substr(0,4)){
								case "card" : Game.cards[t].displayChangeHP(count, newHP, result); break;
								case "hero" : Game[t].displayChangeHP(count, newHP); break;
							}
						}
					}
				},

				"spellInferno" : function(spellResult){

					for(var target in spellResult){

						if(spellResult.hasOwnProperty(target)){

							var obj = spellResult[target][0],
								t = obj["target"][0],
								count = obj["count"][0],
								newHP = obj["newHP"][0],
								result = obj["result"][0];

							switch (t.substr(0,4)){
								case "card" : Game.cards[t].displayChangeHP(count, newHP, result); break;
								case "hero" : Game[t].displayChangeHP(count, newHP); break;
							}
						}
					}
				},

				"spellArmageddon" : function(spellResult){

					for(var target in spellResult){

						if(spellResult.hasOwnProperty(target)){

							var obj = spellResult[target][0],
								t = obj["target"][0],
								count = obj["count"][0],
								newHP = obj["newHP"][0],
								result = obj["result"][0];

							switch (t.substr(0,4)){
								case "card" : Game.cards[t].displayChangeHP(count, newHP, result); break;
								case "hero" : Game[t].displayChangeHP(count, newHP); break;
							}
						}
					}
				},

				"spellMediation" : function(spellResult){

					var changePowers = spellResult["changePowers"][0],
						powersId = changePowers["powersId"][0],
						powers = changePowers["powers"][0];

					Game["powers_" + powersId].changePowers(powers, true);
				},

				"spellAcidRain" : function(spellResult){

					var changePowers = spellResult["changePowers"][0],
						powersId = changePowers["powersId"][0],
						powers = changePowers["powers"][0],
						cards = spellResult["cards"][0];

					Game["powers_" + powersId].changePowers(powers, true);

					for(var target in cards){

						if(cards.hasOwnProperty(target)){

							var obj = cards[target][0],
								t = obj["target"][0],
								count = obj["count"][0],
								newHP = obj["newHP"][0],
								result = obj["result"][0];

							switch (t.substr(0,4)){
								case "card" : Game.cards[t].displayChangeHP(count, newHP, result); break;
								case "hero" : Game[t].displayChangeHP(count, newHP); break;
							}
						}
					}
				},

				"spellCallThunder" : function(spellResult){
					for(var target in spellResult){

						if(spellResult.hasOwnProperty(target)){

							var obj = spellResult[target][0],
								t = obj["target"][0],
								count = obj["count"][0],
								newHP = obj["newHP"][0],
								result = obj["result"][0];

							switch (t.substr(0,4)){
								case "card" : Game.cards[t].displayChangeHP(count, newHP, result); break;
								case "hero" : Game[t].displayChangeHP(count, newHP); break;
							}
						}
					}
				},

				"spellLightning" : function(spellResult){
					for(var target in spellResult){

						if(spellResult.hasOwnProperty(target)){

							var obj = spellResult[target][0],
								t = obj["target"][0],
								count = obj["count"][0],
								newHP = obj["newHP"][0],
								result = obj["result"][0];

							switch (t.substr(0,4)){
								case "card" : Game.cards[t].displayChangeHP(count, newHP, result); break;
								case "hero" : Game[t].displayChangeHP(count, newHP); break;
							}
						}
					}
				},

				"spellChainLightning" : function(spellResult){

					for(var target in spellResult){

						if(spellResult.hasOwnProperty(target)){

							var obj = spellResult[target][0],
								t = obj["target"][0],
								count = obj["count"][0],
								newHP = obj["newHP"][0],
								result = obj["result"][0];

							switch (t.substr(0,4)){
								case "card" : Game.cards[t].displayChangeHP(count, newHP, result); break;
								case "hero" : Game[t].displayChangeHP(count, newHP); break;
							}
						}
					}
				},

				"spellTornado" : function(spellResult){
					for(var target in spellResult){

						if(spellResult.hasOwnProperty(target)){

							var obj = spellResult[target][0],
								t = obj["target"][0],
								count = obj["count"][0],
								newHP = obj["newHP"][0],
								result = obj["result"][0];

							switch (t.substr(0,4)){
								case "card" : Game.cards[t].displayChangeHP(count, newHP, result); break;
								case "hero" : Game[t].displayChangeHP(count, newHP); break;
							}
						}
					}
				},

				"spellNatureRitual" : function(spellResult){

					for(var target in spellResult){

						if(spellResult.hasOwnProperty(target)){

							var obj = spellResult[target][0],
								t = obj["target"][0],
								count = obj["count"][0],
								newHP = obj["newHP"][0],
								result = obj["result"][0];

							switch (t.substr(0,4)){
								case "card" : Game.cards[t].displayChangeHP(count, newHP, result); break;
								case "hero" : Game[t].displayChangeHP(count, newHP); break;
							}
						}
					}
				},

				"spellRevival" : function(spellResult){

					var changeHP = spellResult["changeHP"][0],
						id = changeHP["target"][0],
						count = changeHP["count"][0],
						newHP = changeHP["newHP"][0];

					Game[id].displayChangeHP(count, newHP);
				},

				"spellNaturalFury" : function(spellResult){
					var targets = spellResult["targets"][0],
						cards = spellResult["cards"][0];

					for(var cardKey in cards){
						if(cards.hasOwnProperty(cardKey)){
							Game.cards["card_" + cardKey].accent();
						}
					}

					for(var targetkey in targets){
						if(targets.hasOwnProperty(targetkey)){

							var obj = targets[targetkey][0],
								t = obj["target"][0],
								count = obj["count"][0],
								newHP = obj["newHP"][0],
								result = obj["result"][0];

							switch (t.substr(0,4)){
								case "card" : Game.cards[t].displayChangeHP(count, newHP, result); break;
								case "hero" : Game[t].displayChangeHP(count, newHP); break;
							}
						}
					}
				},

				"spellStoneRain" : function(spellResult){

					for(var target in spellResult){

						if(spellResult.hasOwnProperty(target)){

							var obj = spellResult[target][0],
								t = obj["target"][0],
								count = obj["count"][0],
								newHP = obj["newHP"][0],
								result = obj["result"][0];

							switch (t.substr(0,4)){
								case "card" : Game.cards[t].displayChangeHP(count, newHP, result); break;
								case "hero" : Game[t].displayChangeHP(count, newHP); break;
							}
						}
					}
				},
            };

            return Hero;
        }
    )
})(jQuery);
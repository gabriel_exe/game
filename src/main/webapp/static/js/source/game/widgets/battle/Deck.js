(function($){
	define(
		"widgets/battle/Deck",
		[
			"widgets/battle/Card"
		],
		function(Card){

			function Deck(options){
				this.init(options)
			}

			Deck.prototype = {

				"constructor" : "Deck",

				"init" : function(options){
					var defaults = {
							"element" : null,
							"cards" : {}
						},
						namespace = this;

					$.extend(true, this, defaults, options);

					namespace.element.find(".card").each(function(index, element){
						namespace.cards[$(element).data("literal")] = new Card({
							"element": $(element)
						});
					});

					namespace.addEvents();

				},

				"addEvents" : function(){

					var namespace = this;

					namespace.element.find(".card.enabled").on("click", function(){

						Game.MyArmy.deactivateSlots();
						Game.BattleAction.literal = $(this).data("literal");

						if($(this).hasClass("card-spell")){

							var spellTarget = $(this).data("target");

							if(spellTarget == "i"){
								Game.BattleAction.actionType = "castSpell";
								Game.BattleAction.target = "100";
								Game.BattleAction.send();
								Game.MyArmy.deactivateSlots();
								Game.MyDeck.deactivateAllCards();
								Game.MyDeck.removeEvents();
							} else if(spellTarget == "m"){
								Game.MyDeck.deactivateAllCards();
								$(this).addClass("active");
								Game.BattleAction.actionType = "castSpell";
								Game.MyArmy.activateSlots("castSpell");
							} else if(spellTarget == "e"){
								Game.MyDeck.deactivateAllCards();
								$(this).addClass("active");
								Game.BattleAction.actionType = "castSpell";
								Game.EnemyArmy.activateSlots("castSpell");
							}

						} else {
							Game.MyDeck.deactivateAllCards();
							$(this).addClass("active");
							Game.BattleAction.actionType = "setCard";
							Game.MyArmy.activateSlots("setCard");
						}
					});
				},

				"removeEvents" : function(){
					var namespace = this;

					namespace.element
						.find(".card.enabled")
						.each(function(index, element){
							$(element)
								.removeClass("enabled")
								.addClass("disabled")
								.off("click");
						});
				},

				"deactivateAllCards" : function(){
					var namespace = this;

					namespace.element.find(".card.active").each(function(){
						$(this).removeClass("active");
					});
				},

				"updateAvailableToActionCards" : function(literals){

					var namespace = this,
						cards = literals.split(", ");

					for(var i = 0; i < cards.length; i++){
						$("[data-literal=" + cards[i] + "]", namespace.element)
							.removeClass("disabled")
							.addClass("enabled");
					}

					namespace.addEvents();
				}
			};

			return Deck;
		}
	)
})(jQuery);
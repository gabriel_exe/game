package com.tap5.hotelbooking.dao;

import org.apache.tapestry5.hibernate.annotations.CommitAfter;

import java.io.Serializable;
import java.util.List;
import java.util.Map;


public interface CrudServiceDAO
{

    @CommitAfter
    <T> T create(T t);


    @CommitAfter
    <T> T update(T t);


    @CommitAfter
    <T, PK extends Serializable> void delete(Class<T> type, PK id);


    <T, PK extends Serializable> T find(Class<T> type, PK id);


    <T> List<T> findWithNamedQuery(String queryName);


    <T> List<T> findWithNamedQuery(String queryName, Map<String, Object> params);


    <T> T findUniqueWithNamedQuery(String queryName);

    <T> T findUniqueWithNamedQuery(String queryName, Map<String, Object> params);
}

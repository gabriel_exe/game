package com.tap5.hotelbooking.dao;


import com.tap5.hotelbooking.enums.EPowers;
import com.tap5.hotelbooking.entities.Powers;
import com.tap5.hotelbooking.services.Util;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.TreeMap;


public class PowersDao {
	public void addPowers(Powers powers) throws SQLException{
		Statement statement = Util.getStatement();

		for (String string : powers.getSQLInsertStrings()){
			statement.addBatch(string);
		}

		statement.executeBatch();
	}

	public Powers getPowersByPowersId(int powersId) throws SQLException{

		String string = "SELECT * FROM powers WHERE powersId=" +powersId;

		Powers powers = new Powers();
		powers.setPowersId(powersId);

		Statement statement = Util.getStatement();
		ResultSet resultSet = statement.executeQuery(string);

		while (resultSet.next()){
			TreeMap<String, Object> power = new TreeMap<>();
			EPowers ePower = EPowers.valueOf(resultSet.getString("element"));

			power.put("powersId", powersId);
			power.put("element", resultSet.getString("element"));
			power.put("current", resultSet.getInt("current"));
			power.put("growth", resultSet.getInt("growth"));
			power.put("rusElement", ePower.getRusElement());
			power.put("position", ePower.getPosition());

			powers.getPowers().put(resultSet.getString("element"), power);
		}

		return powers;
	}
}

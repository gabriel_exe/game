package com.tap5.hotelbooking.dao;


import com.tap5.hotelbooking.services.Speller;
import com.tap5.hotelbooking.entities.Hero;
import com.tap5.hotelbooking.services.Factory;
import com.tap5.hotelbooking.services.Util;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;


public class HeroDao {
	public List<Hero> getHeroesByEmailOrName(String email, String name) throws SQLException {
		List<Hero> heroes = new ArrayList<Hero>();

		try {
			String str = "SELECT * FROM heroes WHERE (" +
					"email = \"" + email + "\" " +
					"OR " +
					"name = \"" + name + "\")";

			Statement statement = Util.getStatement();
			ResultSet resultSet = statement.executeQuery(str);

			while (resultSet.next()){
				Hero hero = defineHero(resultSet);
				heroes.add(hero);
			}

		} catch (Exception e){
			e.printStackTrace();
		}

		return heroes;
	}

	public void addHero(Hero hero) throws SQLException{
		String str = "INSERT INTO heroes" +
			"(heroId, userId, name, photo, currentHP, heroClass, level, exp, needExp, cash)" +
			"VALUES" +
			"(" +
				hero.getHeroId() + ", " +
				hero.getUserId() + ", " +
				"'" + hero.getName() + "', " +
				"'" + hero.getPhoto() + "', " +
				hero.getCurrentHP() + ", " +
				"'" + hero.getHeroClass() + "', " +
				hero.getLevel() + ", " +
				hero.getExp() + ", " +
				hero.getNeedExp() + ", " +
				hero.getCash() +
			")";

		Statement statement = Util.getStatement();
		statement.executeUpdate(str);
		statement.close();
	};

	public Hero defineHero(ResultSet resultSet) throws SQLException{

		Hero hero = new Hero();

		hero.setHeroId(resultSet.getInt("heroId"));
		hero.setUserId(resultSet.getInt("userId"));
		hero.setName(resultSet.getString("name"));
		hero.setPhoto(resultSet.getString("photo"));
		hero.setCurrentHP(resultSet.getInt("currentHP"));
		hero.setHeroClass(resultSet.getString("heroClass"));
		hero.setLevel(resultSet.getInt("level"));
		hero.setExp(resultSet.getInt("exp"));
		hero.setNeedExp(resultSet.getInt("needExp"));
		hero.setCash(resultSet.getInt("cash"));

		return hero;

	}

	public Hero getHeroByCriteria(String column, Object value) throws SQLException{
		Factory factory = Factory.getInstance();
		DeckDao deckDao = factory.getDeckDao();
		PowersDao powersDao = factory.getPowersDao();
		ArmyDao armyDao = factory.getArmyDao();

		Hero hero = null;

		String str = "SELECT * FROM heroes WHERE " + column + " = \'" + value + "\'";

		try(Statement statement = Util.getStatement();
			ResultSet resultSet = statement.executeQuery(str);){

			while (resultSet.next()){

				hero = defineHero(resultSet);

				hero.setSpeller(new Speller(hero));

				if(resultSet.getInt("powersId") == 0){
					hero.setPowers(null);
				} else {
					hero.setPowers(powersDao.getPowersByPowersId(resultSet.getInt("powersId")));
					hero.getPowers().setHero(hero);
				}

				if(resultSet.getInt("armyId") == 0){
					hero.setArmy(null);
				} else {
					hero.setArmy(armyDao.getArmyByArmyId(resultSet.getInt("armyId")));
					hero.getArmy().setHero(hero);
				}

				if(resultSet.getInt("deckId") == 0){
					hero.setDeck(null);
				} else {
					hero.setDeck(deckDao.getDeckByDeckId(resultSet.getInt("deckId")));
					hero.getDeck().setHero(hero);
				}
			}
		}

		return hero;
	}
}

package com.tap5.hotelbooking.dao;

import com.tap5.hotelbooking.enums.ECards;
import com.tap5.hotelbooking.entities.Deck;
import com.tap5.hotelbooking.entities.DeckCard;
import com.tap5.hotelbooking.services.Util;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.TreeMap;


public class DeckDao {

	public void addDeck(Deck deck) throws SQLException{

		Statement statement = Util.getStatement();
		String query = "INSERT INTO deck_cards (deckId, number, literal) VALUES ";

		for (DeckCard card : deck.getCards().values()){
			query += card.getSQLInsertValuesString();
		}

		statement.executeUpdate(query.substring(0, query.length() - 2));
	}

	public Deck getDeckByDeckId(int deckId) throws SQLException{

		Deck deck = new Deck();
		deck.setDeckId(deckId);
		deck.setCards(new TreeMap<>());


		Statement statement = Util.getStatement();
		String string = "SELECT * FROM deck_cards WHERE deckId=" + deckId + " ORDER BY number";
		ResultSet resultSet = statement.executeQuery(string);


		while (resultSet.next()){
			DeckCard card = new DeckCard(ECards.valueOf(resultSet.getString("literal")), deck);
			deck.getCards().put(resultSet.getInt("number"), card);
		}

		return deck;
	}
}

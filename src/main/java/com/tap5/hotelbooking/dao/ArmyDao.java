package com.tap5.hotelbooking.dao;


import com.tap5.hotelbooking.enums.ECards;
import com.tap5.hotelbooking.entities.Army;
import com.tap5.hotelbooking.entities.ArmyCard;
import com.tap5.hotelbooking.services.Util;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;


public class ArmyDao {
	public Army getArmyByArmyId(int armyId) throws SQLException{

		Army army = new Army(7);
		army.setArmyId(armyId);

		String string = "SELECT * FROM army_cards WHERE armyId=" + armyId + " ORDER BY position";
		Statement statement = Util.getStatement();
		ResultSet resultSet = statement.executeQuery(string);


		while (resultSet.next()){
			ArmyCard card = new ArmyCard(ECards.valueOf(resultSet.getString("literal")), resultSet.getInt("cardId"), army,
				resultSet.getInt("position"), resultSet.getInt("damage"), resultSet.getInt("currentHP"), false);

			army.getCards()[resultSet.getInt("position")] = card;
		}

		return army;
	}
}

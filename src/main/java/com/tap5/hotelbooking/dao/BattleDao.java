package com.tap5.hotelbooking.dao;


import com.tap5.hotelbooking.entities.Battle;
import com.tap5.hotelbooking.entities.Hero;
import com.tap5.hotelbooking.services.Authenticator;
import com.tap5.hotelbooking.services.Factory;
import com.tap5.hotelbooking.services.Util;
import org.apache.tapestry5.ioc.annotations.Inject;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;


public class BattleDao {

	@Inject
	private Authenticator authenticator;

	Factory factory = Factory.getInstance();
	HeroDao heroDao = factory.getHeroDao();
	DeckDao deckDao = factory.getDeckDao();
	PowersDao powersDao = factory.getPowersDao();


	public boolean checkBattleByHeroId(int id) throws SQLException{
		String string = "SELECT * FROM battles " +
						"WHERE " +
							"(inviterId='" + id + "' " +
								"OR " +
							"guestId='" + id + "') " +
						"AND status!=1";

		Statement statement = Util.getStatement();
		ResultSet resultSet = statement.executeQuery(string);

		return (resultSet.next());
	}

	public void addBattle(Battle battle) throws SQLException{


		Statement statement = Util.getStatement();
		statement.addBatch(battle.getInsertString());

		statement.addBatch(battle.getForward().getSQLUpdateDeckIdString());
		statement.addBatch(battle.getForward().getSQLUpdatePowersIdString());
		statement.addBatch(battle.getForward().getSQLUpdateBattleArmyIdString());

		statement.addBatch(battle.getDefender().getSQLUpdateDeckIdString());
		statement.addBatch(battle.getDefender().getSQLUpdatePowersIdString());
		statement.addBatch(battle.getDefender().getSQLUpdateBattleArmyIdString());

		for (String string : battle.getSQLPowersUpdateStrings()){
			statement.addBatch(string);
		}

		statement.executeBatch();

		deckDao.addDeck(battle.getForward().getDeck());
		deckDao.addDeck(battle.getDefender().getDeck());

		powersDao.addPowers(battle.getForward().getPowers());
		powersDao.addPowers(battle.getDefender().getPowers());

	}

	public Battle getBattleByHeroId(int heroId) throws SQLException{

		Battle battle;
		String string = "SELECT * FROM battles WHERE inviterId='" + heroId + "' OR guestId='" + heroId + "'";

		try(Connection connection = Util.getConnection();
			Statement statement = connection.createStatement();
			ResultSet resultSet = statement.executeQuery(string);) {

			battle = new Battle();

			while (resultSet.next()){

				battle.setBattleId(resultSet.getInt("battleId"));
				battle.setStatus(resultSet.getInt("status"));
				battle.setForwardId(resultSet.getInt("forwardId"));
				battle.setDefenderId(resultSet.getInt("defenderId"));

				Hero forward = heroDao.getHeroByCriteria("heroId", resultSet.getInt("forwardId"));
				Hero defender = heroDao.getHeroByCriteria("heroId", resultSet.getInt("defenderId"));

				battle.setForward(forward);
				battle.getForward().setBattle(battle);
				battle.setDefender(defender);
				battle.getDefender().setBattle(battle);
			}
		}


		return battle;
	}

	public void updateBattle(Battle battle) throws SQLException{
		//Statement statement = Util.getStatement();

		Connection connection = Util.getConnection();
		connection.setAutoCommit(false);
		Statement statement = connection.createStatement();

		for(String string : battle.getSQLUpdateStrings()){
			statement.executeUpdate(string);
			System.out.println(string);
		}

		connection.commit();
	}
}

package com.tap5.hotelbooking.pages;


import com.tap5.hotelbooking.dao.BattleDao;
import com.tap5.hotelbooking.dao.HeroDao;
import com.tap5.hotelbooking.entities.*;
import com.tap5.hotelbooking.services.Authenticator;
import com.tap5.hotelbooking.services.Factory;
import com.tap5.hotelbooking.services.Util;
import org.apache.tapestry5.annotations.Component;
import org.apache.tapestry5.annotations.InjectPage;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.corelib.components.Form;
import org.apache.tapestry5.corelib.components.TextField;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.json.JSONObject;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;


public class Invite{

	@InjectPage
	private Battlefield field;

	@Component(id = "inviteForm")
	private Form form;

	@Component(id = "nameField")
	private TextField nameField;

	@Inject
	private Authenticator authenticator;

	@Property
	private String name;


	Factory factory = Factory.getInstance();
	BattleDao battleDao = factory.getBattleDao();
	HeroDao heroDao = factory.getHeroDao();



	public boolean isFighting() throws SQLException{

		User user = authenticator.getLoggedUser();

		Statement statement = Util.getStatement();

		ResultSet resultSet = statement.executeQuery("SELECT * FROM heroes WHERE userId=" + user.getId());

		resultSet.next();

		return battleDao.checkBattleByHeroId(resultSet.getInt("heroId"));
	}



	void onValidateFromInviteForm() throws SQLException{

		if(name == null || name.trim().equals("")){
			form.recordError(nameField, "Введите имя героя");
		} else {

			Hero guest = heroDao.getHeroByCriteria("name", name);

			if (guest == null){
				form.recordError(nameField, "Герой не найден");
			} else if (guest.getArmy() != null){
				form.recordError(nameField, "Герой уже участвует в битве");
			} else {
				User user = authenticator.getLoggedUser();

				Hero inviter = heroDao.getHeroByCriteria("userId", user.getId());

				Battle battle = new Battle();
				battle.setBattleId(Util.getRandomInRange(100_000_000, 999_999_999));
				battle.setForward(inviter);
				battle.setDefender(guest);
				battle.setInviterId(inviter.getHeroId());
				battle.setGuestId(guest.getHeroId());

				battle.setForwardId(inviter.getHeroId());
				battle.setDefenderId(guest.getHeroId());

				battle.getForward().setDeck(new Deck(4));
				battle.getForward().getDeck().setHero(inviter);
				battle.getDefender().setDeck(new Deck(4));
				battle.getDefender().getDeck().setHero(guest);

				battle.getForward().setPowers(new Powers(true));
				battle.getDefender().setPowers(new Powers(true));

				battle.setJustActionResult(JSONObject.quote(new JSONObject().toString()));

				battleDao.addBattle(battle);
			}
		}
	}


	Object onSuccess() {
		return field;
	}


}

package com.tap5.hotelbooking.pages;

import com.tap5.hotelbooking.dao.BattleDao;
import com.tap5.hotelbooking.dao.HeroDao;
import com.tap5.hotelbooking.entities.*;
import com.tap5.hotelbooking.services.Authenticator;
import com.tap5.hotelbooking.services.Factory;
import com.tap5.hotelbooking.services.IllegalActionException;
import com.tap5.hotelbooking.services.Util;
import org.apache.tapestry5.annotations.Cached;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.services.Request;
import org.apache.tapestry5.util.TextStreamResponse;
import org.json.JSONObject;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.TreeMap;



public class Battlefield {

	@Property
	private Card card;

	@Property
	private DeckCard deckCard;

	@Property
	private ArmyCard armyCard;

	@Property
	private String key;

	@Property
	private String element;

	@Property
	private TreeMap<String, Object> power;

	@Inject
	private Request request;

	@Inject
	private Authenticator authenticator;




	public boolean isFighting() throws SQLException{

		User user = authenticator.getLoggedUser();

		Statement statement = Util.getStatement();

		ResultSet resultSet = statement.executeQuery("SELECT * FROM heroes WHERE userId=" + user.getId());

		resultSet.next();

		return battleDao.checkBattleByHeroId(resultSet.getInt("heroId"));
	}


	Factory factory = Factory.getInstance();
	BattleDao battleDao = factory.getBattleDao();
	HeroDao heroDao = factory.getHeroDao();

	@Cached
	public User getUser(){
		return authenticator.isLoggedIn() ? authenticator.getLoggedUser() : null;
	};

	public boolean isMyAction() throws  SQLException{

		return (this.getHero().getHeroId() == this.getBattle().getForwardId());
	}

	public String getCssClass() throws SQLException{
		return ( this.getHero().getHeroId() == this.getBattle().getForwardId()) ? "" : "disabled";
	}

	@Cached
	public Battle getBattle() throws SQLException{
		User user = this.getUser();

		Hero hero = heroDao.getHeroByCriteria("userId", user.getId());
		return battleDao.getBattleByHeroId(hero.getHeroId());
	}

	@Cached
	public Hero getEnemyHero() throws SQLException{
		Battle battle = getBattle();
		return (battle.getForward().getUserId() == getUser().getId()) ? battle.getDefender() : battle.getForward();
	}

	@Cached
	public Hero getMyHero() throws SQLException{
		Battle battle = getBattle();
		return  (battle.getForward().getUserId() == getUser().getId()) ? battle.getForward(): battle.getDefender();
	}

	public Hero getHero() throws SQLException{
		return heroDao.getHeroByCriteria("userId", getUser().getId());
	}

	@SuppressWarnings("unchecked")
	public TextStreamResponse onCheckActionOpponent(){

		try(Statement statement = Util.getStatement()){
			int heroId = Integer.parseInt(request.getParameter("heroId"));
			String str = "SELECT * FROM battles WHERE inviterId=" + heroId + " OR guestId=" + heroId;
			ResultSet resultSet = statement.executeQuery(str);


			resultSet.first();
			int battleId = resultSet.getInt("battleId");
			int forwardId = resultSet.getInt("forwardId");
			int defenderId = resultSet.getInt("defenderId");

			String status = (forwardId == heroId) ? "done" : "wait";


			if(resultSet.getInt("status") == 1){
				Statement statement1 = Util.getStatement();
				statement1.addBatch("DELETE FROM battles WHERE battleid=" + battleId);
				statement1.executeBatch();
			}

			org.json.simple.JSONObject result = new org.json.simple.JSONObject();

			result.put("status", status);
			if(status.equals("done")){
				result.put("actionResult", resultSet.getString("justActionResult"));
			}

			return new TextStreamResponse("text/plain", result.toJSONString());
		}
		catch (Exception e){

			org.json.simple.JSONObject result = new org.json.simple.JSONObject();

			result.put("status", "error");
			result.put("errorType", "applicationError");
			result.put("errorMessage", e.getMessage());

			return new TextStreamResponse("text/plain", result.toJSONString());
		}



	}

	@SuppressWarnings("unchecked")
	public TextStreamResponse onHeroAction(){

		try {

			User user = this.getUser();
			Battle battle = battleDao.getBattleByHeroId(Integer.parseInt(request.getParameter("heroId")));
			JSONObject result = battle.getForward().action(request, battle);

			return new TextStreamResponse("text/plain", result.toString());

		} catch (IllegalActionException e) {

			org.json.simple.JSONObject result = new org.json.simple.JSONObject();
			result.put("status", "error");
			result.put("errorType", "illegalAction");
			result.put("errorMessage", e.getMessage());

			e.printStackTrace();
			return new TextStreamResponse("text/plain", result.toJSONString());

		} catch (Exception e){

			org.json.simple.JSONObject result = new org.json.simple.JSONObject();
			result.put("status", "error");
			result.put("errorType", "applicationError");
			result.put("errorMessage", e.getMessage());

			e.printStackTrace();
			return new TextStreamResponse("text/plain", result.toJSONString());

		}
	}
}

package com.tap5.hotelbooking.enums;


public enum EPowers {

	fire(0, "Огонь"),
	water(1, "Вода"),
	air(2, "Воздух"),
	earth(3, "Земля");

	private int position;
	private String rusElement;

	EPowers(int position, String rusElement) {
		this.position = position;
		this.rusElement = rusElement;
	}

	public int getPosition() {
		return position;
	}

	public void setPosition(int position) {
		this.position = position;
	}

	public String getRusElement() {
		return rusElement;
	}

	public void setRusElement(String rusElement) {
		this.rusElement = rusElement;
	}
}

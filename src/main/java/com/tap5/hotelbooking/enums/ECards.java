package com.tap5.hotelbooking.enums;


public enum ECards {

	//Fire
	GoblinBerserk(101, "fire", "creatures", "Гоблин берсеркер", "cardOpposite", 1, 16, -4, "Гоблин берсеркер каждый ход наносит 2 урона соседним существам хозяина."),
	FireWall(102, "fire", "creatures", "Стена огня", "cardOpposite", 2, 5, 0, "Когда стена огня призвана, она наносит 5 урона всем существам противника." ),
	PriestOfFire(103, "fire", "creatures", "Жрец огня", "cardOpposite", 3, 13, -3, "Жрец огня увеличивает на 1 прирост Силы Огня хозяина." ),
	FireDragon(104, "fire", "creatures", "Огненный дракончик", "cardOpposite", 4, 18, -4, "Огненный дракончик атакует на том же ходу, что и призван." ),
	OrcLeader(105, "fire", "creatures", "Орк-предводитель", "cardOpposite", 5, 16, -3, "Орк предводитель увеличивает атаку соседних существ хозяина на 2." ),
	FlameWave(106, "fire", "spell", "Огненная волна", "i", 6, 0, 0, "Наносит 9 урона существам противника." ),
	MinotaurCommander(107, "fire", "creatures", "Командир минотавров", "cardOpposite", 7, 20, -6, "Командир минотавров увеличивает на 1 атаку всех остальных существ хозяина (кроме стен)." ),
	Bargul(108, "fire", "creatures", "Баргул", "cardOpposite", 8, 26, -8, "Когда баргул призван, он наносит 4 урона всем остальным существам" ),
	Inferno(109, "fire", "spell", "Инферно", "e", 9, 0, 0, "Наносит 18 урона выбранному существу и 10 урона всем остальным существам противника." ),
	FireElemental(110, "fire", "creatures", "Огненный элементаль", "cardOpposite", 10, 40, 0, "Когда огненный элементаль призван, он наносит 3 урона противнику и его существам. Увеличивает на 1 прирост Силы Огня хозяина." ),
	Armageddon(111, "fire", "spell", "Армагеддон", "i", 11, 0, 0, "Наносит (8+сила Огня мага) урона противнику и всем существам в игре." ),
	Dragon(112, "fire", "creatures", "Дракон", "cardOpposite", 12, 40, -9, "Дракон увеличивает на 50% весь урон, нанесенный заклинаниями хозяина (итоговый урон будет округлен вверх)." ),

	//Water
	Meditation(201, "water", "spell", "Медитация", "i", 1, 0, 0, "Увеличивает магу силу Огня, Воздуха и Земли на 1." ),
	WaterSpirit(202, "water", "creatures", "Водный дух", "cardOpposite", 2, 22, -5, "Водный дух наносит 2 урона хозяину каждый ход." ),
	MerfolkApostate(203, "water", "creatures", "Мерфолк отступник", "cardOpposite", 3, 10, -3, "Когда мерфолк отступник призван, он увеличивает Силу Огня хозяина на 2." ),
	IceGolem(204, "water", "creatures", "Ледяной голем", "cardOpposite", 4, 12, -4, "Ледяной голем не получает уронаот заклинаний." ),
	MerfolkSage(205, "water", "creatures", "Мерфолк мудрец", "cardOpposite", 5, 16, -3, "Мерфолк мудрец увеличивает прирост Силы Воздуха хозяина на 1." ),
	IceGuard(206, "water", "creatures", "Ледяной страж", "cardOpposite", 6, 20, -3, "Ледяной страж уменьшает на 50% весь урон, нанесенный хозяину. (итоговый урон будет округлен вверх)." ),
	HugeTurtle(207, "water", "creatures", "Огромная черепаха", "cardOpposite", 7, 16, -5, "Весь урон, нанесенный по огромной черепахе, уменьшается на 5." ),
	AcidRain(208, "water", "spell", "Кислотный дождь", "i", 8, 0, 0, "Наносит 15 урона по всем существам. Уменьшает на 1 все Силы соперника." ),
	MerfolkLeader(209, "water", "creatures", "Мерфолк предводитель", "cardOpposite", 9, 35, -7, "Мерфолк предводитель позволяет существам хозяина, призванным в соседние слоты, атаковать в том же ходу, в котором они призваны." ),
	WaterElemental(210, "water", "creatures", "Водный элементаль", "cardOpposite", 10, 38, 0, "Когда водный элементаль призван, он лечит хозяину 10 жизни. Увеличивает на 1 прирост Силы Воды хозяина." ),
	MindMaster(211, "water", "creatures", "Властелин магии", "cardOpposite", 11, 23, -6, "Властелин магии увеличивает на 1 прирост всех Сил хозяина." ),
	AstralGuard(212, "water", "creatures", "Астральный страж", "cardOpposite", 12, 17, -1, "Астральный страж уменьшает на 1 прирост всех Сил противника." ),

	//Air
	Faerie(301, "air", "creatures", "Фея", "cardOpposite", 1, 10, -4, "Фея ученик увеличивает на 1 урон всех заклинаний хозяина." ),
	Griffin(302, "air", "creatures", "Грифон", "cardOpposite", 2, 15, -3, "При входе в игру - наносит 5 урона противнику." ),
	CallThunder(303, "air", "spell", "Призыв грома", "e", 3, 0, 0, "Наносит 6 урона выбранному существу противника и самому противнику." ),
	FaerieElder(304, "air", "creatures", "Фей старец", "cardOpposite", 4, 19, -4, "Когда фей старец призван, он лечит хозяину количество здоровья, равное Силе Земли хозяина, но не более 10 здоровья." ),
	WallOfLightning(305, "air", "creatures", "Стена молний", "none", 5, 28, 0, "Стена молний наносит 4 урона противнику каждый ход." ),
	Lightning(306, "air", "spell", "Молния", "i", 6, 0, 0, "Наносит (5+Сила Воздуха мага) урона противнику." ),
	Phoenix(307, "air", "creatures", "Феникс", "cardOpposite", 7, 20, -7, "Каждый раз, когда феникс умирает, если Сила Огня хозяина 10 либо выше, то феникс возрождается." ),
	ChainLightning(308, "air", "spell", "Цепная молния", "i", 8, 0, 0, "Наносит 9 урона противнику и всем его существам." ),
	LightningCloud(309, "air", "creatures", "Грозовое облако", "allEnemies", 9, 20, -4, "При атаке грозовая туча наносит урон сразу всем существам противника и самому противнику." ),
	Tornado(310, "air", "spell", "Торнадо", "e", 10, 0, 0, "Уничтожает выбранное существо противника." ),
	AirElemental(311, "air", "creatures", "Воздушный элементаль", "cardOpposite", 11, 44, -9, "Когда воздушный элементаль призван, он наносит 8 урона противнику. Увеличивает прирост Силы Воздуха хозяина на 1." ),
	Titan(312, "air", "creatures", "Титан", "cardOpposite", 12, 45, -9, "Когда титан призван, он наносит 15 урона существу противника в слоте напротив." ),

	//Earth
	ElvenHealer(401, "earth", "creatures", "Эльф-целитель", "cardOpposite", 1, 10, -2, "Эльф целитель лечит 3 жизни хозяину каждый ход." ),
	NatureRitual(402, "earth", "spell", "Природный ритуал", "m", 2, 0, 0, "Природный ритуал лечит 8 жизни выбранному существу и магу, сыгравшему это заклинание." ),
	ForestSprite(403, "earth", "creatures", "Лесной дух", "allEnemies", 3, 22, -1, "При атаке лесной дух наносит урон сразу всем существам противника и самому противнику." ),
	Revival(404, "earth", "spell", "Возрождение", "i", 4, 0, 0, "Маг исцеляется на количество здоровья, равное его удвоенной Силе Земли." ),
	ElfHermit(405, "earth", "creatures", "Эльф отшельник", "cardOpposite", 5, 13, -1, "Эльф отшельник увеличивает прирост Силы Земли хозяина на 2." ),
	NaturalFury(406, "earth", "spell", "Природная ярость", "i", 6, 0, 0, "Наносит противнику урон, равный суммарной атаке двух сильнейших существ игрока, сыгравшего это заклинание." ),
	GiantSpider(407, "earth", "creatures", "Гигантский паук", "cardOpposite", 7, 24, -4, "Когда огромный паук призван, он размещает в соседние слоты лесных пауков (2/11)." ),
	Troll(408, "earth", "creatures", "Троль", "cardOpposite", 8, 25, -6, "Тролль восполняет себе 4 единицы здоровья каждый ход." ),
	StoneRain(409, "earth", "spell", "Каменный дождь", "i", 9, 0, 0, "Наносит 25 урона каждому существу." ),
	EarthElemental(410, "earth", "creatures", "Элементаль Земли", "cardOpposite", 10, 50, 0, "Земляной элементаль увеличивает прирост Силы Земли хозяина на 1." ),
	MasterHealer(411, "earth", "creatures", "Верховный целитель", "cardOpposite", 11, 35, -3, "Каждый ход верховный целитель лечит 3 жизни хозяину и всем его существам." ),
	Hydra(412, "earth", "creatures", "Гидра", "allEnemies", 12, 40, -3, "При атаке гидра наносит урон сразу всем существам противника и самому противнику. Восполняет себе 4 единицы здоровья каждый ход." ),

	ForestSpider(413, "earth", "creatures", "Лесной паук", "cardOpposite", 999, 11, -2, "Обычный такой лесной паук");


	private int number;
	private String element;
	private String type;
	private String name;
	private String target;
	private int cost;
	private int maxHP;
	private int damage;
	private String description;


	ECards(int number, String element, String type, String name, String target, int cost, int maxHP, int damage, String description) {
		this.number = number;
		this.element = element;
		this.type = type;
		this.name = name;
		this.target = target;
		this.cost = cost;
		this.maxHP = maxHP;
		this.damage = damage;
		this.description = description;
	}

	public int getNumber() {
		return number;
	}

	public void setNumber(int number) {
		this.number = number;
	}

	public String getElement() {
		return element;
	}

	public void setElement(String element) {
		this.element = element;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTarget() {
		return target;
	}

	public void setTarget(String target) {
		this.target = target;
	}

	public int getCost() {
		return cost;
	}

	public void setCost(int cost) {
		this.cost = cost;
	}

	public int getMaxHP() {
		return maxHP;
	}

	public void setMaxHP(int maxHP) {
		this.maxHP = maxHP;
	}

	public int getDamage() {
		return damage;
	}

	public void setDamage(int damage) {
		this.damage = damage;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}

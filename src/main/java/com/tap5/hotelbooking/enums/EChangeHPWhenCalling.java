package com.tap5.hotelbooking.enums;

import java.util.HashSet;

/**
 * Created by gabriel.exe on 04.09.2015.
 */
public enum EChangeHPWhenCalling {

	FireWall(102, "enemyArmy", -5),
	Bargul(108, "allCards", -4),
	FireElemental(110, "allEnemies", -3),

	WaterElemental(210, "myHero", 10),
	
	Griffin(302, "enemyHero", -5),
	FaerieElder(304, "myHero", 999),
	AirElemental(311, "enemyHero", -8),
	Titan(312, "cardOpposite", -15);

	private int number;
	private String target;
	private int count;


	EChangeHPWhenCalling(int number, String target, int count) {
		this.number = number;
		this.target = target;
		this.count = count;
	}

	public int getNumber() {
		return number;
	}

	public void setNumber(int number) {
		this.number = number;
	}

	public String getTarget() {
		return target;
	}

	public void setTarget(String target) {
		this.target = target;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}




	public static HashSet<String> getLiterals(){
		HashSet<String> literals = new HashSet<>();

		for(EChangeHPWhenCalling card : values()){
			literals.add(card.name());
		}

		return literals;
	}
}

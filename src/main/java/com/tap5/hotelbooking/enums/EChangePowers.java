package com.tap5.hotelbooking.enums;

import java.util.HashSet;

/**
 * Created by gabriel.exe on 04.09.2015.
 */
public enum EChangePowers {

	PriestOfFire(102, "calling-death", "myPowers", "fire", "growth", 1),
	FireElemental(110, "calling-death", "myPowers", "fire", "growth", 1),
	MerfolkApostate(202, "calling", "myPowers", "fire", "current", 2),
	MerfolkSage(205, "calling-death", "myPowers", "air", "growth", 1),
	WaterElemental(210, "calling-death", "myPowers", "water", "growth", 1),
	MindMaster(211, "calling-death", "myPowers", "all", "growth", 1),
	AstralGuard(212, "calling-death", "enemyPowers", "all", "growth", -1),
	AirElemental(311, "calling-death", "myPowers", "air", "growth", 1),
	ElfHermit(405, "calling-death", "myPowers", "earth", "growth", 2),
	EarthElemental(410, "calling-death", "myPowers", "earth", "growth", 1);


	private int number;
	private String situations;
	private String target;
	private String element;
	private String type;
	private int count;

	EChangePowers(int number, String situations, String target, String element, String type, int count) {
		this.number = number;
		this.situations = situations;
		this.target = target;
		this.element = element;
		this.type = type;
		this.count = count;
	}

	public int getNumber() {
		return number;
	}

	public void setNumber(int number) {
		this.number = number;
	}

	public String getSituations() {
		return situations;
	}

	public void setSituations(String situations) {
		this.situations = situations;
	}

	public String getTarget() {
		return target;
	}

	public void setTarget(String target) {
		this.target = target;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getElement() {
		return element;
	}

	public void setElement(String element) {
		this.element = element;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}





	public static HashSet<String> getLiterals(){

		HashSet<String> literals = new HashSet<>();

		for(EChangePowers card : values()){
			literals.add(card.name());
		}

		return literals;
	}



}

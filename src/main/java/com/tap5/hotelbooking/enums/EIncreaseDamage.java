package com.tap5.hotelbooking.enums;


import java.util.HashSet;

public enum EIncreaseDamage {

	OrcLeader("neighbors", 2),
	MinotaurCommander("myArmy", 1);

	private String target;
	private int count;

	EIncreaseDamage(String target, int count) {
		this.target = target;
		this.count = count;
	}

	public String getTarget() {
		return target;
	}

	public void setTarget(String target) {
		this.target = target;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public static HashSet<String> getLiterals(){
		HashSet<String> literals = new HashSet<>();

		for(EIncreaseDamage card : values()){
			literals.add(card.name());
		}

		return literals;
	}
}

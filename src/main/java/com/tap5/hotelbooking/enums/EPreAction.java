package com.tap5.hotelbooking.enums;

import java.util.HashSet;


public enum EPreAction {

	//damage
	GoblinBerserk(101, "neighbors", -1),
	WaterSpirit(202, "myHero", -2),
	WallOfLightning(305, "enemyHero", -4),

	//regeneration
	ElvenHealer(401, "myHero", 3),
	Troll(408, "card", 4),
	MasterHealer(411, "allFriends", 3),
	Hydra(412, "card", 3);

	private int number;
	private String target;
	private int count;

	EPreAction(int number, String target, int count) {
		this.number = number;
		this.target = target;
		this.count = count;
	}


	public int getNumber() {
		return number;
	}

	public void setNumber(int number) {
		this.number = number;
	}

	public String getTarget() {
		return target;
	}

	public void setTarget(String target) {
		this.target = target;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}




	public static HashSet<String> getLiterals(){

		HashSet<String> literals = new HashSet<>();

		for(EPreAction preAction : values()){
			literals.add(preAction.name());
		}

		return literals;
	}
}

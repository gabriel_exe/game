package com.tap5.hotelbooking.components;


import com.tap5.hotelbooking.pages.Start;
import com.tap5.hotelbooking.services.Authenticator;
import org.apache.tapestry5.annotations.Log;
import org.apache.tapestry5.ioc.annotations.Inject;


public class MainMenu {

	@Inject
	private Authenticator authenticator;

	@Log
	public Object onActionFromLogout()
	{
		authenticator.logout();
		return Start.class;
	}
}

package com.tap5.hotelbooking.components.forms;

import com.tap5.hotelbooking.dao.CrudServiceDAO;
import com.tap5.hotelbooking.dao.QueryParameters;
import com.tap5.hotelbooking.dao.HeroDao;
import com.tap5.hotelbooking.entities.User;
import com.tap5.hotelbooking.pages.Overview;
import com.tap5.hotelbooking.security.AuthenticationException;
import com.tap5.hotelbooking.services.Authenticator;
import com.tap5.hotelbooking.entities.Hero;
import com.tap5.hotelbooking.services.Factory;
import org.apache.tapestry5.EventConstants;
import org.apache.tapestry5.annotations.Component;
import org.apache.tapestry5.annotations.OnEvent;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.beaneditor.Validate;
import org.apache.tapestry5.corelib.components.Form;
import org.apache.tapestry5.ioc.Messages;
import org.apache.tapestry5.ioc.annotations.Inject;

import java.sql.SQLException;


public class Signup
{

    @Property
    @Validate("username")
    private String username;

    @Property
    @Validate("required,email")
    private String email;

    @Property
    @Validate("password")
    private String password;

    @Property
    @Validate("password")
    private String verifyPassword;


	@Property
	private String heroName;

	@Property
	private String heroPhoto;

	@Property
	private String heroClass;

    @Inject
    private CrudServiceDAO crudServiceDAO;

    @Component
    private Form registerForm;

    @Inject
    private Messages messages;

    @Inject
    private Authenticator authenticator;

    @OnEvent(value = EventConstants.VALIDATE, component = "RegisterForm")
    public void checkForm()
    {
        if (!verifyPassword.equals(password))
        {
            registerForm.recordError(messages.get("error.verifypassword"));
        }
    }

    @OnEvent(value = EventConstants.SUCCESS, component = "RegisterForm")
    public Object proceedSignup() throws SQLException
    {

        User userVerif = crudServiceDAO.findUniqueWithNamedQuery(
                User.BY_USERNAME_OR_EMAIL,
                QueryParameters.with("username", username).and("email", email).parameters());

        if (userVerif != null)
        {
            registerForm.recordError(messages.get("error.userexists"));

            return null;
        }

        User user = new User(username, email, password);

        crudServiceDAO.create(user);

		//Создание порвого героя для только что зарегистрированного пользователя
		Hero hero = new Hero(user.getId(), heroName, heroPhoto, heroClass);
		Factory factory = Factory.getInstance();
		HeroDao heroDao = factory.getHeroDao();
		heroDao.addHero(hero);


        try
        {
            authenticator.login(username, password);
        }
        catch (AuthenticationException ex)
        {
            registerForm.recordError("Authentication process has failed");
            return this;
        }

        return Overview.class;
    }

	public String getFirstPhoto(){
		return "face1.jpg";
	}

	public String[] getPhotosList(){

		String[] photosList = new String[19];

		for (int i = 0; i <= 18; i++){
			photosList[i] = "face" + (i + 1) + ".jpg";
		}

		return photosList;
	}
}

package com.tap5.hotelbooking.components.forms;

import com.tap5.hotelbooking.dao.HeroDao;
import com.tap5.hotelbooking.pages.Invite;
import com.tap5.hotelbooking.entities.Hero;
import com.tap5.hotelbooking.services.Factory;
import org.apache.tapestry5.annotations.Component;
import org.apache.tapestry5.annotations.InjectPage;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.corelib.components.Form;
import org.apache.tapestry5.corelib.components.PasswordField;
import org.apache.tapestry5.corelib.components.TextField;

import java.sql.SQLException;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class FormRegistration {


	@InjectPage
	private Invite invite;

	@Property
	private String email;

	@Property
	private String password;

	@Property
	private String name;




	@Component(id = "form_registration")
	private Form form;

	@Component(id = "email")
	private TextField fieldEmail;

	@Component(id = "password")
	private PasswordField fieldPassword;

	@Component(id = "name")
	private TextField fieldName;

	private boolean emailOk;

	private boolean nameOk;

	private boolean passwordOk;

	private List<Hero> heroes;

	Factory factory = Factory.getInstance();
	HeroDao heroDao = factory.getHeroDao();


	public void onValidateFromForm_registration() throws SQLException{

		if (email == null || email.trim().equals("")) {
			form.recordError(fieldEmail, "Введите e-mail");
		} else if (!validEmail(email)){
			form.recordError(fieldEmail, "Некорректный e-mail");
		} else if (email.length() > 30) {
			form.recordError(fieldEmail, "Максимальная длинна 30 символов");
		} else {
			emailOk = true;
		}



		if (name == null || name.trim().equals("")) {
			form.recordError(fieldName, "Введите имя героя");
		} else if(!validName(name)){
			form.recordError(fieldName, "Некорректное имя");
		} else if(name.length() > 30){
			form.recordError(fieldName, "Максимальная длинна 30 символов");
		} else if(name.length() < 2){
			form.recordError(fieldName, "Минимальная длинна 2 символа");
		} else {
			nameOk = true;
		}


		if (password == null || password.trim().equals("")) {
			form.recordError(fieldPassword, "Введите пароль");
		} else if(password.length() < 6) {
			form.recordError(fieldPassword, "Минимальная длинна 6 символов");
		} else if(password.length() > 12) {
			form.recordError(fieldPassword, "Максимальная длинна 12 символов");
		}


		if(emailOk && nameOk ){
			heroes = heroDao.getHeroesByEmailOrName(email, name);

			if(heroes != null){

				if(!checkEmail(email)){
					form.recordError(fieldEmail, "Данный e-mail уже зарегистрирован");
				}

				if(!checkName(name)){
					form.recordError(fieldName, "Данный имя уже используется");
				}
			}
		}
	}

	Object onSuccess() throws SQLException{
		HeroDao heroDao = factory.getHeroDao();
		Hero hero = new Hero();
		heroDao.addHero(hero);
		return Invite.class;
	}


	public boolean validEmail(String email){
		Pattern p = Pattern.compile("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
		Matcher m = p.matcher(email);

		return m.matches();
	}

	public boolean validName(String name){
		Pattern p = Pattern.compile("[A-z]+");
		Matcher m = p.matcher(name);

		return m.matches();
	}

	public boolean checkEmail(String email){
		for (Hero hero : heroes){
			/*if(hero.getEmail().equals(email)){
				return false;
			}*/
		}

		return true;
	}

	public boolean checkName(String name){
		for (Hero hero : heroes){
			if(hero.getName().equals(name)){
				return false;
			}
		}

		return true;
	}

}

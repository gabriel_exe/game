package com.tap5.hotelbooking.services;


import com.tap5.hotelbooking.dao.*;


public final class Factory {

	public static Factory instance = new Factory();

	public BattleDao battleDao;
	public HeroDao heroDao;
	public DeckDao deckDao;
	public PowersDao powersDao;
	public ArmyDao armyDao;

	private Factory(){}

	public static Factory getInstance(){
		return Factory.instance;
	}

	public HeroDao getHeroDao(){

		if(heroDao == null){
			heroDao = new HeroDao();
		}

		return heroDao;
	}


	public BattleDao getBattleDao(){
		if(battleDao == null){
			battleDao = new BattleDao();
		}

		return battleDao;
	}

	public DeckDao getDeckDao(){
		if(deckDao == null){
			deckDao = new DeckDao();
		}

		return deckDao;
	}

	public PowersDao getPowersDao(){
		if(powersDao == null){
			powersDao = new PowersDao();
		}

		return powersDao;
	}

	public ArmyDao getArmyDao(){
		if(armyDao == null){
			armyDao = new ArmyDao();
		}

		return armyDao;
	}

}

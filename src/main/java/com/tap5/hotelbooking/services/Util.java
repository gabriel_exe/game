package com.tap5.hotelbooking.services;

import com.mysql.fabric.jdbc.FabricMySQLDriver;

import java.sql.*;
import java.util.Random;

public class Util{

	private final static String HOST = "jdbc:mysql://localhost:3306/game";
	private final static String USER = "root";
	private final static String PASSWORD = "root";

	public static int getRandomInRange(int min, int max){
		Random random = new Random();
		return random.nextInt((max - min) + 1) + min;
	}

	public static Connection getConnection() throws SQLException {
		Driver driver = new FabricMySQLDriver();
		return DriverManager.getConnection(HOST, USER, PASSWORD);
	}

	public static Statement getStatement() throws SQLException {
		Driver driver = new FabricMySQLDriver();
		Connection connection = DriverManager.getConnection(HOST, USER, PASSWORD);
		return  connection.createStatement();
	}

	public static void clearBattle() throws SQLException{
		Statement statement = Util.getStatement();
		statement.addBatch("UPDATE heroes SET deckId=null, battlePowersId=null, armyId=null WHERE heroId IN (100000, 999999)");
		statement.addBatch("TRUNCATE TABLE battles");

		statement.addBatch("TRUNCATE TABLE decks");
		statement.addBatch("TRUNCATE TABLE stacks");
		statement.addBatch("DELETE FROM cards WHERE entityType='deck'");

		statement.addBatch("TRUNCATE TABLE battlePowers");




		statement.executeBatch();

	}
}

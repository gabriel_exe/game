package com.tap5.hotelbooking.services;

import com.tap5.hotelbooking.entities.ArmyCard;
import com.tap5.hotelbooking.entities.Hero;
import com.tap5.hotelbooking.entities.Powers;
import org.json.JSONException;
import org.json.JSONObject;


public class Speller {

	private Hero hero;

	public Hero getHero() {
		return hero;
	}

	public void setHero(Hero hero) {
		this.hero = hero;
	}

	public Speller() {}

	public Speller(Hero hero) {
		this.hero = hero;
	}

	public int calculateDamage(int count){

		double absolute = 0;
		double percentage = 100;

		for(ArmyCard card : hero.getArmy().getCards()){
			
			if(card != null && card.getLiteral().equals("Faerie")){
				absolute += 1.0;
			}

			if(card != null && card.getLiteral().equals("Dragon")){
				percentage += 50.0;
			}
		}

		return (int) Math.ceil(Math.abs(count) * percentage/100.0 + absolute) * -1;
	}

	public JSONObject apply(String literal, int target) throws JSONException{

		JSONObject result;

		switch (literal){
			case "FlameWave" : result = flameWave(); break;
			case "Inferno" : result = inferno(target); break;
			case "Armageddon" : result = armageddon(); break;
			case "Meditation" : result = meditation(); break;
			case "AcidRain" : result = acidRain(); break;
			case "CallThunder" : result = callThunder(target); break;
			case "Lightning" : result = lightning(); break;
			case "ChainLightning" : result = chainLightning(); break;
			case "Tornado" : result = tornado(target); break;
			case "NatureRitual" : result = natureRitual(target); break;
			case "Revival" : result = revival(); break;
			case "NaturalFury" : result = naturalFury(); break;
			case "StoneRain" : result = stoneRain(); break;
			default: result = new JSONObject(); break;
		}

		return result;
	}

	public JSONObject flameWave() throws JSONException{
		JSONObject result =  new JSONObject();

		for (ArmyCard card : hero.getBattle().getDefender().getArmy().getCards()){
			if(card != null && !card.getLiteral().equals("IceGolem")){
				result.append(Integer.toString(card.getCardId()), card.changeHP(calculateDamage(-9)));
			}
		}

		return result;
	}

	public JSONObject inferno(int target) throws JSONException{
		JSONObject result =  new JSONObject();

		for(ArmyCard card : hero.getBattle().getDefender().getArmy().getCards()){
			if(card != null && !card.getLiteral().equals("IceGolem")){
				int damage = (card.getPosition() == target) ? calculateDamage(-18) : calculateDamage(-10);
				result.append(Integer.toString(card.getCardId()), card.changeHP(damage));
			}
		}

		return result;
	}

	public JSONObject armageddon() throws JSONException{
		JSONObject result =  new JSONObject();

		int fire = (int) hero.getPowers().getPowers().get("fire").get("current");
		int damage = calculateDamage(-(fire + 5));
		Hero enemy = hero.getBattle().getDefender();

		result.append("hero_" + enemy.getHeroId(), enemy.changeHP(damage));

		for (ArmyCard card : hero.getArmy().getCards()){
			if(card != null && !card.getLiteral().equals("IceGolem")){
				result.append(Integer.toString(card.getCardId()), card.changeHP(damage));
			}
		}

		for (ArmyCard card : hero.getBattle().getDefender().getArmy().getCards()){
			if(card != null && !card.getLiteral().equals("IceGolem")){
				result.append(Integer.toString(card.getCardId()), card.changeHP(damage));
			}
		}

		return result;
	}

	public JSONObject meditation() throws JSONException{

		JSONObject result = new JSONObject();
		JSONObject powersResult = new JSONObject();
		Powers powers = hero.getPowers();

		powersResult.append("fire", powers.changePower("fire", "current", 1));
		powersResult.append("air", powers.changePower("air", "current", 1));
		powersResult.append("earth", powers.changePower("earth", "current", 1));

		result.append("powersId", powers.getPowersId());
		result.append("powers", powersResult);

		return new JSONObject().append("changePowers", result);
	}

	public JSONObject acidRain() throws JSONException{

		JSONObject result = new JSONObject();
		JSONObject powers = new JSONObject();
		JSONObject cards = new JSONObject();

		int damage = calculateDamage(-15);

		powers.append("powersId", hero.getBattle().getDefender().getPowers().getPowersId());
		powers.append("powers", hero.getBattle().getDefender().getPowers().changePowers("current", -1));

		for (ArmyCard card : hero.getArmy().getCards()){
			if(card != null && !card.getLiteral().equals("IceGolem")){
				cards.append(Integer.toString(card.getCardId()), card.changeHP(damage));
			}
		}

		for (ArmyCard card : hero.getBattle().getDefender().getArmy().getCards()){
			if(card != null && !card.getLiteral().equals("IceGolem")){
				cards.append(Integer.toString(card.getCardId()), card.changeHP(damage));
			}
		}

		result.append("changePowers", powers);
		result.append("cards", cards);

		return result;
	}

	public JSONObject callThunder(int target) throws JSONException{

		JSONObject result = new JSONObject();

		Hero enemy = hero.getBattle().getDefender();
		ArmyCard card = enemy.getArmy().getCards()[target];
		int damage = calculateDamage(-6);

		result.append("card_" + card.getCardId(), card.changeHP(damage));
		result.append("hero_" + enemy.getHeroId(), enemy.changeHP(damage));

		return result;
	}

	public JSONObject lightning() throws JSONException{

		JSONObject result = new JSONObject();

		int air = (int) hero.getPowers().getPowers().get("air").get("current");
		int damage = calculateDamage(-(air + 5));
		Hero enemy = hero.getBattle().getDefender();


		result.append("hero_" + enemy.getHeroId(), enemy.changeHP(damage));

		return result;
	}

	public JSONObject chainLightning() throws JSONException{

		JSONObject result = new JSONObject();

		int damage = calculateDamage(-9);

		for (ArmyCard card : hero.getBattle().getDefender().getArmy().getCards()){
			if(card != null && !card.getLiteral().equals("IceGolem")){
				result.append(Integer.toString(card.getCardId()), card.changeHP(damage));
			}
		}

		result.append("hero_" + hero.getBattle().getDefender().getHeroId(), hero.getBattle().getDefender().changeHP(damage));

		return result;
	}

	public JSONObject tornado(int target) throws JSONException{

		JSONObject result = new JSONObject();

		ArmyCard card = hero.getBattle().getDefender().getArmy().getCards()[target];
		int hp = card.getCurrentHP();
		result.append("card_" + card.getCardId(), card.changeHP(-hp));

		return result;

	}

	public JSONObject natureRitual(int target) throws JSONException{

		JSONObject result = new JSONObject();

		ArmyCard card = hero.getArmy().getCards()[target];
		result.append("card_" + card.getCardId(), card.changeHP(8));
		result.append("hero_" + hero.getHeroId(), hero.changeHP(8));

		return result;

	}

	public JSONObject revival() throws JSONException{

		JSONObject result = new JSONObject();
		int earth = (int) hero.getPowers().getPowers().get("earth").get("current");
		result.append("id", hero.getHeroId());
		result.append("changeHP", hero.changeHP(earth*2));
		return result;
	}

	public JSONObject naturalFury() throws JSONException{

		JSONObject result = new JSONObject();
		JSONObject cards = new JSONObject();
		JSONObject targets = new JSONObject();

		int damage = 0;

		for(ArmyCard card : hero.getArmy().getCardsForNaturalFury()){
			if(card != null){
				damage += card.getDamage();
				cards.append(String.valueOf(card.getCardId()), "lol");
			}
		}

		Hero enemyHero = hero.getBattle().getDefender();

		targets.append("hero_" + enemyHero.getHeroId(), enemyHero.changeHP(calculateDamage(damage)));

		result.append("cards", cards);
		result.append("targets", targets);

		return result;
	}

	public JSONObject stoneRain() throws JSONException{

		JSONObject result = new JSONObject();
		int damage = calculateDamage(-25);

		for (ArmyCard card : hero.getArmy().getCards()){
			if(card != null && !card.getLiteral().equals("IceGolem")){
				result.append(Integer.toString(card.getCardId()), card.changeHP(damage));
			}
		}

		for (ArmyCard card : hero.getBattle().getDefender().getArmy().getCards()){
			if(card != null && !card.getLiteral().equals("IceGolem")){
				result.append(Integer.toString(card.getCardId()), card.changeHP(damage));
			}
		}

		return result;
	}


}

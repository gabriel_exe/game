package com.tap5.hotelbooking.services;


public class IllegalActionException extends Exception{

    public final static String BROKEN_DATA_SET = "Недостаточно данных для совершения хода";
    public final static String BROKEN_SEQUENCE = "Ход принадлежит другому игроку";
    public final static String CARD_MISSING = "Карта отсутствует в колоде";
    public final static String INSUFFICIENT_POWER = "Недостаточно силы для совершения действия";
	public final static String SLOT_DOES_NOT_EXIST = "Слот с данным номером не существует";
    public final static String ILLEGAL_TARGET_CARD = "Цель для существа указана неверно";
    public final static String ILLEGAL_TARGET_SPELL_ENEMY = "Цель для заклинания указана неверно(только на карты соперника, кроме Ледяного голема)";
    public final static String ILLEGAL_TARGET_SPELL_MY = "Цель для заклинания указана неверно(только на свои карты)";



    public IllegalActionException() {
    }

    public IllegalActionException(String message) {
        super(message);
    }

    public IllegalActionException(String message, Throwable cause) {
        super(message, cause);
    }

    public IllegalActionException(Throwable cause) {
        super(cause);
    }

    public IllegalActionException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}

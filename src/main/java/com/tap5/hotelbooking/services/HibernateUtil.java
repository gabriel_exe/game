package com.tap5.hotelbooking.services;

import com.tap5.hotelbooking.entities.Hero;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import java.util.Properties;


public class HibernateUtil {

	private static SessionFactory sessionFactory;

	private HibernateUtil(){}

	static {
		try{
			Properties prop= new Properties();
			prop.setProperty("hibernate.dialect", "org.hibernate.dialect.MySQLDialect");
			prop.setProperty("hibernate.driver_class", "com.mysql.jdbc.Driver");
			prop.setProperty("hibernate.connection.url", "jdbc:mysql://localhost:3306/game");
			prop.setProperty("hibernate.connection.username", "root");
			prop.setProperty("hibernate.connection.password", "root");


			sessionFactory = new Configuration()
				.addProperties(prop)
				//.addPackage("ru.company.game.tables")
				//.addAnnotatedClass(Battle.class)
				//.addAnnotatedClass(Card.class)
				.addAnnotatedClass(Hero.class)
				//.addAnnotatedClass(Deck.class)
				//.addAnnotatedClass(Stack.class)
				.buildSessionFactory();
		} catch (Throwable e){
			throw new ExceptionInInitializerError(e);
		}

	}

	public static SessionFactory getSessionFactory(){
		return sessionFactory;
	}
}

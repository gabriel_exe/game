package com.tap5.hotelbooking.entities;

import com.tap5.hotelbooking.enums.EPreAction;
import org.json.JSONException;
import org.json.JSONObject;

import java.sql.SQLException;
import java.util.HashSet;


public class Army {

	private int armyId;

	private int heroId;

	private ArmyCard[] cards;

	private Hero hero;



	public Army(int size){
		this.cards = new ArmyCard[size];
	}



	public int getArmyId() {
		return armyId;
	}

	public void setArmyId(int armyId) {
		this.armyId = armyId;
	}

	public ArmyCard[] getCards() {
		return cards;
	}

	public void setCards(ArmyCard[] cards) {
		this.cards = cards;
	}

	public int getHeroId() {
		return heroId;
	}

	public void setHeroId(int heroId) {
		this.heroId = heroId;
	}

	public Hero getHero() {
		return hero;
	}

	public void setHero(Hero hero) {
		this.hero = hero;
	}








	public JSONObject getAttackResult() throws JSONException, SQLException{

		JSONObject result = new JSONObject();
		JSONObject cards = new JSONObject();

		Battle battle = this.getHero().getBattle();
		Hero defender = battle.getDefender();

		for(ArmyCard card : this.cards){
			if(card != null
				&& (!card.isJustSummoned() || card.getLiteral().equals("FireDragon") || card.isNeighborsWithMerfolkLeader())
				&& card.getDamage() != 0){

				cards.append(Integer.toString(card.getPosition()), card.attack());

				if(defender.getCurrentHP() <= 0) {
					break;
				}
			}
		}

		result.append("cards", cards);
		result.append("armyAttackResult", (defender.getCurrentHP() <= 0) ? battle.finalizeBattle("defenderDead") : "continues");

		return result;
	}

	public JSONObject preAction() throws JSONException{

		JSONObject result = new JSONObject();

		HashSet<String> literals = EPreAction.getLiterals();

		for(ArmyCard card : cards){
			if(card != null && literals.contains(card.getLiteral())){

				String target = EPreAction.valueOf(card.getLiteral()).getTarget();
				int count = EPreAction.valueOf(card.getLiteral()).getCount();
				Hero myHero = card.getArmy().getHero();
				Hero enemyHero = myHero.getBattle().getForward();
				JSONObject targets = new JSONObject();


				switch (target) {
					case "card" : targets.append("card_" + card.getCardId(), card.changeHP(count)); break;
					case "myHero" : targets.append("hero_" + myHero.getHeroId(), myHero.changeHP(count)); break;
					case "enemyHero" : targets.append("hero_" + enemyHero.getHeroId(), enemyHero.changeHP(count)); break;
					case "allFriends" : targets.append("hero_" + myHero.getHeroId(), myHero.changeHP(count));
						for (ArmyCard c : card.getArmy().getCards()){
							if(c != null){
								targets.append("card_" + c.getCardId(), c.changeHP(count));
							}
						}
						break;
					case "neighbors" :
						int armyPosition = card.getPosition();
						if(card.getPosition() != 0 && getCards()[armyPosition - 1] != null){
							ArmyCard leftNeighbor = this.getCards()[armyPosition - 1];
							targets.append("card_" + leftNeighbor.getCardId(), leftNeighbor.changeHP(count));
						}

						if(card.getPosition() != 6 && this.getCards()[armyPosition + 1] != null){
							ArmyCard rightNeighbor = this.getCards()[armyPosition + 1];
							targets.append("card_" + rightNeighbor.getCardId(), rightNeighbor.changeHP(count));
						}
						break;
				}

					JSONObject object = new JSONObject();
					object.append("accentCard", "card_" + card.getCardId());
					object.append("targets", targets);

					result.append(Integer.toString(card.getPosition()), object);
			}
		}

		return result;
	}

	public JSONObject updateElementalDamage(String element) throws JSONException{

		Powers powers = hero.getPowers();
		JSONObject cards = new JSONObject();
		String elementalLiterals = "FireElementalWaterElementalAirElementalEarthElemental";

		for(ArmyCard card : getCards()){
			if(card != null && elementalLiterals.contains(card.getLiteral()) && card.getElement().equals(element)){
 				card.setDamage(-(int) powers.getPowers().get(element).get("current"));
				cards.append(String.valueOf(card.getCardId()), card.getPositiveDamage());
			}
		}

		return cards;
	}

	public String getSQLInsertValues(){

		String string = "";

		for(ArmyCard card : cards){
			if(card != null){
				string +=
					"(" +
						card.getCardId() + ", " +
						"'" + card.getLiteral() + "', " +
						armyId + ", " +
						card.getPosition() + ", " +
						card.getDamage() + ", " +
						card.getCurrentHP() +
					"),";
			}

		}


		return !string.equals("") ? string.substring(0, string.length() - 1) : "";
	}

	public int getFreeSlotsCount(){
		int count = 0;

		for(ArmyCard card : cards){
			if(card == null){
				count++;
			}
		}
		return count;
	}

	public ArmyCard[] getCardsForNaturalFury(){

		ArmyCard[] cards = new ArmyCard[2];

		for (ArmyCard card : hero.getArmy().getCards()){
			if(card != null && Math.abs(card.getDamage()) > 0){

				if(cards[0] == null){
					cards[0] = card;
				}
				else if(cards[1] == null){
					cards[1] = card;
				}
				else if(Math.abs(cards[0].getDamage()) < Math.abs(card.getDamage())){
					cards[0] = card;
				}
				else if(Math.abs(cards[1].getDamage()) < Math.abs(card.getDamage())){
					cards[1] = card;
				}
			}
		}

		return cards[0] != null ? cards : null;
	}
}

package com.tap5.hotelbooking.entities;

import com.tap5.hotelbooking.enums.ECards;


public abstract class Card {


	// base constants
	private String literal;

	private int number;

	private String element;

	private String type;

	private String name;

	private String description;

	private int cost;

	private int maxHP;

	private int damage;

	private String target;


	public Card() {}

	public Card(ECards cardEnum){
		this.literal = cardEnum.name();
		this.number = cardEnum.getNumber();
		this.element = cardEnum.getElement();
		this.type = cardEnum.getType();
		this.name = cardEnum.getName();
		this.description = cardEnum.getDescription();
		this.cost = cardEnum.getCost();
		this.maxHP = cardEnum.getMaxHP();
		this.damage = cardEnum.getDamage();
		this.target = cardEnum.getTarget();
	}



	public Integer getNumber() {
		return number;
	}

	public void setNumber(Integer number) {
		this.number = number;
	}

	public String getElement() {
		return element;
	}

	public void setElement(String element) {
		this.element = element;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getLiteral() {
		return literal;
	}

	public void setLiteral(String literal) {
		this.literal = literal;
	}

	public int getCost() {
		return cost;
	}

	public void setCost(int cost) {
		this.cost = cost;
	}

	public int getMaxHP() {
		return maxHP;
	}

	public void setMaxHP(int maxHP) {
		this.maxHP = maxHP;
	}

	public int getDamage() {
		return damage;
	}

	public void setDamage(int damage) {
		this.damage = damage;
	}

	public String getTarget() {
		return target;
	}

	public void setTarget(String target) {
		this.target = target;
	}
}

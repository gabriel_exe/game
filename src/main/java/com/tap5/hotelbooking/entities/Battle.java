package com.tap5.hotelbooking.entities;


import org.json.JSONException;
import org.json.JSONObject;

import java.sql.SQLException;
import java.util.ArrayList;


public class Battle {

	private int battleId;

	private int status;

	private int inviterId;

	private int guestId;

	private Hero forward;

	private Hero defender;

	private int forwardId;

	private int defenderId;

	private String justActionResult;








	public int getBattleId() {
		return battleId;
	}

	public void setBattleId(int battleId) {
		this.battleId = battleId;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public long getInviterId() {
		return inviterId;
	}

	public void setInviterId(int inviterId) {
		this.inviterId = inviterId;
	}

	public long getGuestId() {
		return guestId;
	}

	public void setGuestId(int guestId) {
		this.guestId = guestId;
	}

	public Hero getForward() {
		return forward;
	}

	public void setForward(Hero forward) {
		this.forward = forward;
	}

	public Hero getDefender() {
		return defender;
	}

	public void setDefender(Hero defender) {
		this.defender = defender;
	}

	public int getForwardId() {
		return forwardId;
	}

	public void setForwardId(int forwardId) {
		this.forwardId = forwardId;
	}

	public int getDefenderId() {
		return defenderId;
	}

	public void setDefenderId(int defenderId) {
		this.defenderId = defenderId;
	}

	public String getJustActionResult() {
		return justActionResult;
	}

	public void setJustActionResult(String justActionResult) {
		this.justActionResult = justActionResult;
	}







	public String getInsertString(){
		return "INSERT INTO battles " +
			"(" +
				"battleId, " +
				"inviterId, " +
				"guestId, " +
				"forwardId, " +
				"defenderId" +
			") " +
			"VALUES " +
			"(" +
				this.getBattleId() + ", " +
				this.getInviterId() + ", " +
				this.getGuestId() + ", " +
				this.getForwardId() + ", " +
				this.getDefenderId() +
			")";
	}

	public String getSQLUpdateString(){
		return "UPDATE battles SET " +
			"status=" + status + ", " +
			"forwardId=" + forwardId + ", " +
			"defenderId=" + defenderId + ", " +
			"justActionResult=" + justActionResult +
			" WHERE battleId=" + battleId;
	}

	public ArrayList<String> getSQLUpdateStrings(){
		ArrayList<String> strings = new ArrayList<>();

		strings.add(getSQLUpdateString());
		strings.addAll(forward.getSQLUpdateStrings());
		strings.addAll(defender.getSQLUpdateStrings());
		strings.addAll(getSQLPowersUpdateStrings());
		strings.addAll(getSQLArmyUpdateStrings());

		if (status == 1) {
			strings.add("DELETE FROM deck_cards WHERE deckId IN (" + forward.getDeck().getDeckId() + ", " + defender.getDeck().getDeckId() + ")");
			strings.add("UPDATE heroes SET armyId=null, deckId=null, powersId=null WHERE heroId IN (" + forward.getHeroId() + ", " + defender.getHeroId() + ")");
		}

		return strings;
	}


	public JSONObject finalizeBattle(String reason) throws JSONException, SQLException{

		JSONObject result = new JSONObject();

		result.append("reason", reason);

		if (reason.equals("defenderDead")){
			result.append("winner", forward.getName());
			result.append("looser", defender.getName());
		} else if(reason.equals("surrender")){
			result.append("winner", defender.getName());
			result.append("looser", forward.getName());
		}


		status = 1;
		forward.setCurrentHP(20);
		defender.setCurrentHP(20);

		return result;
	}

	public ArrayList<String> getSQLPowersUpdateStrings(){

		ArrayList<String> strings = new ArrayList<>();

		int forwardPowersId = forward.getPowers().getPowersId();
		int defenderPowersId = defender.getPowers().getPowersId();
		String forwardValues =  forward.getPowers().getSQLInsertValues();
		String defenderValues =  defender.getPowers().getSQLInsertValues();
		strings.add("DELETE FROM powers WHERE powersId IN (" + forwardPowersId + "," + defenderPowersId + ")");

		/*
		* В случае с завершённой битвой, значения нужно удалить,
		* а вот вставлять по новой не будем.
		* */
		if(status != 1){
			strings.add("INSERT INTO powers (powersId, element, current, growth) VALUES " + forwardValues + "," + defenderValues);
		}


		return strings;
	}

	public ArrayList<String> getSQLArmyUpdateStrings(){

		ArrayList<String> strings = new ArrayList<>();

		int forwardArmyId = forward.getArmy().getArmyId();
		int defenderArmyId = defender.getArmy().getArmyId();
		String forwardValues =  forward.getArmy().getSQLInsertValues();
		String defenderValues =  defender.getArmy().getSQLInsertValues();
		strings.add("DELETE FROM army_cards WHERE armyId IN (" + forwardArmyId + "," + defenderArmyId + ")");

		/*
		* В случае с завершённой битвой, значения нужно удалить,
		* а вот вставлять по новой не будем.
		* */
		if(status != 1){
			String values = "";
			if (!forwardValues.equals("") && !defenderValues.equals("")){
				values += forwardValues + ", " + defenderValues;
			} else if(!forwardValues.equals("") && defenderValues.equals("")){
				values += forwardValues;
			} else if(forwardValues.equals("") && !defenderValues.equals("")){
				values += defenderValues;
			}

			if(!values.equals("")){
				strings.add("INSERT INTO army_cards (cardId, literal, armyId, position, damage, currentHP) VALUES " + values);
			}
		}


		return strings;
	}
}

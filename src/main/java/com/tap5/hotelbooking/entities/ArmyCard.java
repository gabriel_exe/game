package com.tap5.hotelbooking.entities;


import com.tap5.hotelbooking.enums.ECards;
import com.tap5.hotelbooking.enums.EChangeHPWhenCalling;
import com.tap5.hotelbooking.enums.EChangePowers;
import com.tap5.hotelbooking.enums.EIncreaseDamage;
import com.tap5.hotelbooking.services.Util;
import org.json.JSONException;
import org.json.JSONObject;

public class ArmyCard extends Card {

	private int cardId;

	private Army army;

	private int position;

	private int damage;

	private int currentHP;

	private boolean justSummoned;


	public ArmyCard(ECards cardEnum, int cardId, Army army, int position, int damage, int currentHP, boolean justSummoned) {
		super(cardEnum);
		this.cardId = cardId;
		this.army = army;
		this.position = position;
		this.damage = damage;
		this.currentHP = currentHP;
		this.justSummoned = justSummoned;
	}


	public int getCardId() {
		return cardId;
	}

	public void setCardId(int cardId) {
		this.cardId = cardId;
	}

	public Army getArmy() {
		return army;
	}

	public void setArmy(Army army) {
		this.army = army;
	}

	public int getPosition() {
		return position;
	}

	public void setPosition(int position) {
		this.position = position;
	}

	@Override
	public int getDamage() {
		return damage;
	}

	@Override
	public void setDamage(int damage) {
		this.damage = damage;
	}

	public int getCurrentHP() {
		return currentHP;
	}

	public void setCurrentHP(int currentHP) {
		this.currentHP = currentHP;
	}

	public boolean isJustSummoned() {
		return justSummoned;
	}

	public void setJustSummoned(boolean justSummoned) {
		this.justSummoned = justSummoned;
	}

	public int getPositiveDamage(){
		return this.getDamage() * -1;
	}

	public JSONObject attack() throws JSONException{

		JSONObject result = new JSONObject();
		JSONObject targets = new JSONObject();

		Hero enemyHero = army.getHero().getBattle().getDefender();
		Army enemyArmy = enemyHero.getArmy();
		ArmyCard cardOpposite = enemyArmy.getCards()[position];

		switch (getTarget()){
			case "cardOpposite" :
				if(cardOpposite == null){
					targets.append("hero_" + enemyHero.getHeroId(), enemyHero.changeHP(damage));
				} else {
					targets.append("card_" + cardOpposite.getCardId(), cardOpposite.changeHP(damage));
				}
				break;
			case "allEnemies" :
				targets.append("hero_" + enemyHero.getHeroId(), enemyHero.changeHP(damage));

				for (ArmyCard card : enemyArmy.getCards()){
					if(card != null){
						targets.append("card_" + card.getCardId(), card.changeHP(damage));
					}
				}

		}

		result.append("card_" + this.getCardId(), targets);

		return result;
	}

	public JSONObject death() throws JSONException{
		JSONObject result = new JSONObject();

		army.getCards()[position] = null;

		result.append("changePowers", changePowers("death"));
		result.append("increaseDamage", increaseDamage("death"));

		if(getLiteral().equals("Phoenix") && (int)army.getHero().getPowers().getPowers().get("fire").get("current") >= 10){
			ArmyCard card = new ArmyCard(ECards.Phoenix, cardId, army, position, damage, ECards.Phoenix.getMaxHP(), false);
			army.getCards()[position] = card;
			result.append("resurrection", card.getBaseProperties());
		}

		return result;
	}

	/*
	* Абстрактный метод, который изменяет hp карты,
	* как в сторону увеличения так и в сторону уменьшения - все зависит от знака числа.
	* Это сделано для того что бы была одна! "точка доступа" к этому действию,
	* и логика не была раскидана по разным классам.
	* В будущем(введение классов героев и артефактов),
	* возможно придется анализировать "источник" изменения hp.
	* Как вариант, можно будет добавить второй обобщенный аргумент,
	* в который можно передавать этот самый источник в качестве объекта.
	* А пока, достаточно числа.
	* */
	public JSONObject changeHP(int count) throws JSONException{

		int totalCount = (count < 0)
			? reduceDamage(count)
			: ((currentHP + count) > getMaxHP())
				? getMaxHP() - currentHP
				: count;


		JSONObject result = new JSONObject();
		result.append("target", "card_" + cardId);
		result.append("count", totalCount);
		result.append("newHP", (currentHP += totalCount) > 0 ? currentHP : 0);
		result.append("result", (currentHP > 0) ? "survival" : death());


		return result;
	}

	public JSONObject changeHPWhenCalling() throws JSONException{

		JSONObject result = new JSONObject();

		if(EChangeHPWhenCalling.getLiterals().contains(getLiteral())) {

			String target = EChangeHPWhenCalling.valueOf(this.getLiteral()).getTarget();
			int count = EChangeHPWhenCalling.valueOf(this.getLiteral()).getCount();
			Hero myHero = army.getHero();
			Hero enemyHero = army.getHero().getBattle().getDefender();
			Army enemyArmy = enemyHero.getArmy();
			Army myArmy = myHero.getArmy();
			ArmyCard cardOpposite = enemyArmy.getCards()[position];

			switch (target) {
				case "myHero":
					count = (count != 999)
						? count
						: ((int) myHero.getPowers().getPowers().get("earth").get("current") < 10)
							? (int) myHero.getPowers().getPowers().get("earth").get("current")
							: 10;
					result.append("hero_" + myHero.getHeroId(), myHero.changeHP(count));
					break;
				case "enemyHero":
					result.append("hero_" + enemyHero.getHeroId(), enemyHero.changeHP(count));
					break;
				case "enemyArmy":
					for (ArmyCard card : enemyArmy.getCards()) {
						if(card != null){
							result.append("card_" + card.getCardId(), card.changeHP(count));
						}
					}
					break;
				case "cardOpposite":
					if (cardOpposite != null) {
						result.append("card_" + cardOpposite.getCardId(), cardOpposite.changeHP(count));
					}
					break;
				case "allCards" :
					for (ArmyCard card : enemyArmy.getCards()) {
						if(card != null){
							result.append("card_" + card.getCardId(), card.changeHP(count));
						}
					}
					for (ArmyCard card : myArmy.getCards()) {
						if(card != null && !card.isJustSummoned()){
							result.append("card_" + card.getCardId(), card.changeHP(count));
						}
					}
					break;
				case "allEnemies":
					result.append("hero_" + enemyHero.getHeroId(), enemyHero.changeHP(count));

					for (ArmyCard card : enemyArmy.getCards()) {
						if(card != null){
							result.append("card_" + card.getCardId(), card.changeHP(count));
						}
					}
					break;
			}
		}


		return result;
	}

	public JSONObject changePowers(String situation) throws JSONException{

		JSONObject result = new JSONObject();

		if(EChangePowers.getLiterals().contains(this.getLiteral())){

			EChangePowers enumObject = EChangePowers.valueOf(this.getLiteral());
			String situations = enumObject.getSituations();
			String target = enumObject.getTarget();
			String element = enumObject.getElement();
			String type = enumObject.getType();
			// todo тут может быть ошибка, есди неглядя добавить значения в ChangePowers
			// но с существующими значениями все работает
			int count = (situation.equals("calling"))
					? enumObject.getCount()
					: (enumObject.getCount() > 0)
						? enumObject.getCount() * -1
						: Math.abs(enumObject.getCount());

			if(situations.contains(situation)){
				// Для того что бы избежать свича или условия,
				// будем ссылаться на силы относительно текущего хода
				Powers myPowers = this.getArmy().getHero().getPowers();
				Powers enemyPowers = (this.getArmy().getHero().isForward())
						? this.getArmy().getHero().getBattle().getDefender().getPowers()
						: this.getArmy().getHero().getBattle().getForward().getPowers();
				Powers relativePowers = (target.equals("enemyPowers"))
						? enemyPowers
						: myPowers;

				JSONObject powers = (element.equals("all"))
						? relativePowers.changePowers(type, count)
						: new JSONObject().append(element, relativePowers.changePower(element, type, count));

				result.append("powersId", relativePowers.getPowersId());
				result.append("powers", powers);
			}
		}

		return result;
	}

	public JSONObject increaseDamage(String situation) throws JSONException{

		JSONObject result = new JSONObject();

		//Блок для случая когда вызванная карта усиливает другие карты
		if(EIncreaseDamage.getLiterals().contains(getLiteral())){

			EIncreaseDamage enumCard = EIncreaseDamage.valueOf(getLiteral());
			int count = (situation.equals("call")) ? enumCard.getCount() * -1 : enumCard.getCount();

			switch (enumCard.getTarget()){
				case "neighbors" :
					if(position != 0 && position != 6){

						ArmyCard leftCard = army.getCards()[position - 1];
						if(leftCard != null && leftCard.getDamage() != 0){
							leftCard.setDamage(leftCard.getDamage() + count);
							result.append(String.valueOf(leftCard.getCardId()), leftCard.getPositiveDamage());
						}

						ArmyCard rightCard = army.getCards()[position + 1];
						if(rightCard != null &&  rightCard.getDamage() != 0){
							rightCard.setDamage(rightCard.getDamage() + count);
							result.append(String.valueOf(rightCard.getCardId()), rightCard.getPositiveDamage());
						}
					}
					break;
				case "myArmy" :
					for(ArmyCard card : army.getCards()){
						if(card != null && card.getDamage() != 0 && card.getPosition() != position){
							card.setDamage(card.getDamage() + count);
							result.append(String.valueOf(card.getCardId()), card.getPositiveDamage());
						}
					}
			}
		}
		else if(!situation.equals("death")){
			// Блок для ситуации когда вызванная карта усиливается за счёт уже вызванных ранее карт,
			// логично, что это не выполняется в тот момент когда карта умирает,
			// хотя с учтом того что карта так или ниче станет null ничего страшного в выполнении этого куска кода не будет
			int increaseDamage = 0;

			for(ArmyCard card : army.getCards()){
				if(card != null){
					if(card.getLiteral().equals("MinotaurCommander")){
						increaseDamage += -1;
					}

					if(card.getLiteral().equals("OrcLeader")){
						if (card.getPosition() + 1 == position || card.getPosition() -1 == position) {
							increaseDamage += -2;
						}
					}
				}
			}

			damage += increaseDamage;

			result.append(String.valueOf(cardId), getPositiveDamage());
		}

		return result;
	}

	public JSONObject additionalSummon() throws JSONException{

		JSONObject result = new JSONObject();

		if(getLiteral().equals("GiantSpider")){

			JSONObject cards = new JSONObject();
			ECards forestSpider = ECards.ForestSpider;

			if(position != 0 && army.getCards()[position - 1] == null){

				ArmyCard leftSpider = new ArmyCard(forestSpider, Util.getRandomInRange(100_000_000, 999_999_999),
					army, position - 1, forestSpider.getDamage(), forestSpider.getMaxHP(), true);

				army.getCards()[position - 1] = leftSpider;

				JSONObject leftCard = new JSONObject();

				leftCard.append("card", leftSpider.getBaseProperties());
				leftCard.append("increaseDamage", leftSpider.increaseDamage("call"));

				cards.append(String.valueOf(leftSpider.getCardId()), leftCard);
			}

			if(position != 6 && army.getCards()[position + 1] == null){

				ArmyCard rightSpider = new ArmyCard(forestSpider, Util.getRandomInRange(100_000_000, 999_999_999),
					army, position + 1, forestSpider.getDamage(), forestSpider.getMaxHP(), true);

				army.getCards()[position + 1] = rightSpider;

				JSONObject rightCard = new JSONObject();

				rightCard.append("card", rightSpider.getBaseProperties());
				rightCard.append("increaseDamage", rightSpider.increaseDamage("call"));

				cards.append(String.valueOf(rightSpider.getCardId()), rightCard);
			}

			result.append("cards", cards);
		}

		return result;
	}

	public String getPhoto(){
		String[] strings = super.getLiteral().split("(?=\\p{Upper})");
		return (String.join("_", strings) + ".jpg").toLowerCase();
	}

	public int reduceDamage(int damage){

		int reduce = 0;

		if(getLiteral().equals("HugeTurtle")){
			reduce += 5;
		}

		int total = damage + reduce;

		return (total < 0 ) ? total : 0;
	}

	public JSONObject getBaseProperties() throws JSONException{
		JSONObject result = new JSONObject();

		result.append("heroId", army.getHero().getHeroId());
		result.append("armyId", army.getArmyId());
		result.append("slot", position);
		result.append("cardId", cardId);
		result.append("cost", getCost());
		result.append("damage", getPositiveDamage());
		result.append("hp", currentHP);
		result.append("photo", getPhoto());
		result.append("description", getDescription());

		return result;
	}

	/**
	 * <p>
	 *     Проверяет, находиться ли в одном из соседних слотов карта MerfolkLeader.
	 * </p>
	 * @return <i>true</i> если нашелся сосед-MerfolkLeader, <i>false</i> если такого нет
	 */
	public boolean isNeighborsWithMerfolkLeader(){
		return
			position != 0
				&& army.getCards()[position - 1] != null
				&& army.getCards()[position - 1].getLiteral().equals("MerfolkLeader")
			|| position != 6
				&& army.getCards()[position + 1] != null
				&& army.getCards()[position + 1].getLiteral().equals("MerfolkLeader");
	}
}

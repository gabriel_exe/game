package com.tap5.hotelbooking.entities;

import com.tap5.hotelbooking.enums.EPowers;
import com.tap5.hotelbooking.services.Util;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.TreeMap;


public class Powers {

	private int powersId;

	private LinkedHashMap<String, TreeMap<String, Object>> powers;

	private Hero hero;





	public Powers(){
		this.powers = new LinkedHashMap<>();
	}

	public Powers(boolean define) {
		this.powersId = Util.getRandomInRange(100_000_000, 999_999_999);
		this.powers = defineDefaultPowers(this.powersId);
	}




	public int getPowersId() {
		return powersId;
	}

	public void setPowersId(int powersId) {
		this.powersId = powersId;
	}

	public LinkedHashMap<String, TreeMap<String, Object>> getPowers() {
		return powers;
	}

	public void setPowers(LinkedHashMap<String, TreeMap<String, Object>> powers) {
		this.powers = powers;
	}

	public Hero getHero() {
		return hero;
	}

	public void setHero(Hero hero) {
		this.hero = hero;
	}

	public LinkedHashMap<String, TreeMap<String, Object>> defineDefaultPowers(int powersId){

		LinkedHashMap<String, TreeMap<String, Object>> powers = new LinkedHashMap<>();

		powers.put("fire", new TreeMap<>());
		powers.put("water", new TreeMap<>());
		powers.put("air", new TreeMap<>());
		powers.put("earth", new TreeMap<>());

		for(String power : powers.keySet()){
			EPowers ePowers = EPowers.valueOf(power);

			powers.get(power).put("powersId", powersId);
			powers.get(power).put("element", power);
			powers.get(power).put("current", Util.getRandomInRange(2,5));
			powers.get(power).put("growth", 1);
			powers.get(power).put("rusElement", ePowers.getRusElement());
			powers.get(power).put("position", ePowers.getPosition());
		}

		return powers;
	}

	public String getSQLInsertValues(){
		String string = "";

		for(TreeMap<String, Object> power : powers.values()){
			string +=
				"(" +
					power.get("powersId") + ", " +
					"'" + power.get("element") + "', " +
					power.get("current") + ", " +
					power.get("growth") +
				"),";
		}
		return string.substring(0, string.length() - 1);
	}

	public String[] getSQLInsertStrings(){
		String[] result = new String[4];

		int i = 0;
		for (String power : this.getPowers().keySet()){
			result[i] = "INSERT INTO powers " +
				"(" +
					"powersId, " +
					"element, " +
					"current, " +
					"growth" +
				")" +
				"VALUES" +
				"(" +
					this.getPowersId() + ", " +
					"'" + power + "', " +
					this.getPowers().get(power).get("current") + ", " +
					this.getPowers().get(power).get("growth") +
				");";

			i++;
		}

		return result;
	}

	public ArrayList<String> getSQLUpdateStrings(){
		ArrayList<String> result = new ArrayList<String>();

		int i = 0;
		for (String power : this.getPowers().keySet()){
			result.add(
				"UPDATE battlePowers SET " +
					"current=" + this.getPowers().get(power).get("current") + ", " +
					"growth=" + this.getPowers().get(power).get("growth") +
				" WHERE powerId=" + this.getPowers().get(power).get("powerId")
			);

			i++;
		}

		return result;
	}

	// Плюсует силы героя: текущая + прирост
	public JSONObject updatePowersBeforeNewAction() throws JSONException{

		JSONObject result = new JSONObject();
		JSONObject powers = new JSONObject();

		for(TreeMap<String, Object> power : this.getPowers().values()){
			powers.append((String) power.get("element"), changePower((String) power.get("element"), "current", (int) power.get("growth")));
		}

		result.append("powersId", powersId);
		result.append("powers", powers);

		return result;
	}

	// Получает значение сил(текущая + прирост),
	// для обновления их, в браузере, при переходе хода.
	public JSONObject getPowersValues() throws JSONException{

		JSONObject powers = new JSONObject();
		powers.append("powersId", this.getPowersId());

		for(TreeMap<String, Object> power : this.getPowers().values()){

			JSONObject object = new JSONObject();
			object.append("current", power.get("current"));
			object.append("growth", power.get("growth"));

			powers.append((String) power.get("element"), object);
		}

		return powers;
	}


	// Изменяет значение одной силы, возвращает результат изменения в виде объекта
	public JSONObject changePower(String element, String type,  int count) throws JSONException{

		JSONObject result = new JSONObject();

		int newValue = ((int) this.getPowers().get(element).get(type)) + count;
		this.getPowers().get(element).put(type, newValue);

		result.append("element", element);
		result.append("type", type);
		result.append("count", count);
		result.append("newValue", newValue);
		result.append("updateElementalDamage", hero.getArmy().updateElementalDamage(element));

		return result;
	}

	// Изменяет значение всех сил, возвращает результат изменения в виде объекта
	public JSONObject changePowers(String type,  int count) throws JSONException{

		JSONObject result = new JSONObject();

		for(TreeMap<String, Object> power : getPowers().values()){
			result.append((String) power.get("element"), changePower((String) power.get("element"), type, count));
		}

		return result;
	}
}

package com.tap5.hotelbooking.entities;

import com.tap5.hotelbooking.enums.ECards;
import com.tap5.hotelbooking.services.Util;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.TreeMap;


public class Deck {


	private int deckId;

	private TreeMap<Integer, DeckCard> cards;

	private Hero hero;





	public Deck() {}

	public Deck(int size) throws SQLException{
		this.deckId = Util.getRandomInRange(100_000_000, 999_999_999);
		this.cards = defineCards(size);
	}


	public int getDeckId() {
		return deckId;
	}

	public void setDeckId(int deckId) {
		this.deckId = deckId;
	}

	public TreeMap<Integer, DeckCard> getCards() {
		return cards;
	}

	public void setCards(TreeMap<Integer, DeckCard> cards) {
		this.cards = cards;
	}

	public Hero getHero() {
		return hero;
	}

	public void setHero(Hero hero) {
		this.hero = hero;
	}

	public TreeMap<Integer, DeckCard> defineCards(int size) throws SQLException{

		TreeMap<Integer, DeckCard> cards = new TreeMap<>();

		cards.putAll(defineRandomStack("fire", size));
		cards.putAll(defineRandomStack("water", size));
		cards.putAll(defineRandomStack("air", size));
		cards.putAll(defineRandomStack("earth", size));

		return cards;
	}

	public TreeMap<Integer, DeckCard> defineRandomStack(String element, int size) throws SQLException{

		//Создаем список всех карт данного элемента.
		ArrayList<ECards> cards = new ArrayList<>();
		for(ECards card : ECards.values()){
			if(card.getElement().equals(element) && card.getCost() != 999){
				cards.add(card);
			}
		}

		//Выбираем случайную карту, и помещаем её в стек.
		//карту при этом удаляем, что бы не выбрать её еще раз.
		TreeMap<Integer, DeckCard> stack = new TreeMap<>();
		for(int i = 0; i < size; i++){
			int rand = Util.getRandomInRange(0, (cards.size()-1));

			DeckCard card = new DeckCard(ECards.valueOf(cards.get(rand).name()), this);

			stack.put(card.getNumber(), card);
			cards.remove(rand);
		}

		return stack;
	}

	public String getAvailableToActionCards(){

		String string = "";

		for(DeckCard card : cards.values()){
			if(card.isEnabledToAction(true)){
				string += card.getLiteral() + ", ";
			}
		}

		return string.equals("") ? "" : string.substring(0, string.length()-2);
	}

	public TreeMap<Integer, DeckCard> getStackElement(String element){

		TreeMap<Integer, DeckCard> stack = new TreeMap<Integer, DeckCard>();

		for (DeckCard card : cards.values()){
			if(card.getElement().equals(element)){
				stack.put(card.getNumber(), card);
			}
		}

		return stack;
	}

	public LinkedHashSet<String> getElements(){

		LinkedHashSet<String> elements = new LinkedHashSet<>();

		for (DeckCard card : cards.values()){
			if(!elements.contains(card.getElement())){
				elements.add(card.getElement());
			}
		}

		return elements;
	}

	public TreeMap<String, DeckCard> getCardsMap(){
		TreeMap<String, DeckCard> map = new TreeMap<>();

		for (DeckCard card : cards.values()){
			map.put(card.getLiteral(), card);
		}

		return map;
	}
}

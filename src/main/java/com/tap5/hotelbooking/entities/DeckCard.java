package com.tap5.hotelbooking.entities;

import com.tap5.hotelbooking.enums.ECards;


public class DeckCard extends Card {

	private Deck deck;

	public DeckCard(ECards cardEnum, Deck deck) {
		super(cardEnum);
		this.deck = deck;
	}


	public Deck getDeck() {
		return deck;
	}

	public void setDeck(Deck deck) {
		this.deck = deck;
	}

	public String getDamageForDisplay(){

		switch (super.getLiteral()){
			case "FireElemental" :
			case "WaterElemental" :
			case "AirElemental" :
			case "EarthElemental" : return "?";
			default: return String.valueOf(super.getDamage() * -1);
		}
	}



	public String disabledOrEnabled(){
		return isEnabledToAction(false) ? "enabled" : "disabled";
	}

	public boolean isEnabledToAction(boolean ignoreForwardRole){

		Hero owner = deck.getHero();

		if(!ignoreForwardRole && !owner.isForward()){
			return false;
		} else {
			if((int) owner.getPowers().getPowers().get(getElement()).get("current") < getCost()){
				return false;
			}
			else{
				if(getType().equals("creatures")){
					if(deck.getHero().getArmy().getFreeSlotsCount() == 0) {
						return false;
					}
				}
				else {
					if(getTarget().equals("i")){
						if(getLiteral().equals("NaturalFury") && deck.getHero().getArmy().getCardsForNaturalFury() == null){
							return false;
						}
					}
					else if(getTarget().equals("e")){

						int targets = 0;

						for(ArmyCard card : deck.getHero().getBattle().getDefender().getArmy().getCards()){
							if(card != null && !card.getLiteral().equals("IceGolem")){
								targets++;
								break;
							}
						}

						if(targets == 0){
							return false;
						}
					}
					else if(getTarget().equals("m")){

						int targets = 0;

						for(ArmyCard card : deck.getHero().getArmy().getCards()){
							if(card != null){
								targets++;
								break;
							}
						}

						if(targets == 0){
							return false;
						}
					}
				}
			}
		}

		return true;
	}

	public String getPhoto(){
		String[] strings = super.getLiteral().split("(?=\\p{Upper})");
		return (String.join("_", strings) + ".jpg").toLowerCase();
	}


	public String getSQLInsertValuesString(){
		return "(" +
					deck.getDeckId() + ", " +
					super.getNumber() + ", " +
					"\"" + super.getLiteral() + "\"" +
				"), ";
	}
}

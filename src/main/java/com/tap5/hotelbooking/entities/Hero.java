package com.tap5.hotelbooking.entities;

import com.tap5.hotelbooking.enums.ECards;
import com.tap5.hotelbooking.services.Speller;
import com.tap5.hotelbooking.services.Factory;
import com.tap5.hotelbooking.services.IllegalActionException;
import com.tap5.hotelbooking.services.Util;
import org.apache.tapestry5.services.Request;
import org.json.JSONException;
import org.json.JSONObject;

import java.sql.SQLException;
import java.util.ArrayList;



public class Hero {

	private int heroId;

	private int userId;

	private String name;

	private String photo;

	private int currentHP;

	private String heroClass;

	private int level;

	private int exp;

	private int needExp;

	private int cash;

	private Deck deck;

	private Powers powers;

	private Army army;

	private Battle battle;

	private String battleRole;

	private Speller speller;



	// Constructors

	public Hero(){}

	public Hero(int userId, String name, String photo, String heroClass) {
		this.heroId = Util.getRandomInRange(100_000_000, 999_999_999);
		this.userId = userId;
		this.name = name;
		this.photo = photo;
		this.heroClass = heroClass;
		this.currentHP = 50;
		this.level = 1;
		this.exp = 0;
		this.needExp = 10;
		this.cash = 0;
	}



	// Getters and setters
	public int getHeroId() {
		return heroId;
	}

	public void setHeroId(int heroId) {
		this.heroId = heroId;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	public int getCurrentHP() {
		return currentHP;
	}

	public void setCurrentHP(int currentHP) {
		this.currentHP = currentHP;
	}

	public String getHeroClass() {
		return heroClass;
	}

	public void setHeroClass(String heroClass) {
		this.heroClass = heroClass;
	}

	public int getLevel() {
		return level;
	}

	public void setLevel(int level) {
		this.level = level;
	}

	public int getExp() {
		return exp;
	}

	public void setExp(int exp) {
		this.exp = exp;
	}

	public int getNeedExp() {
		return needExp;
	}

	public void setNeedExp(int needExp) {
		this.needExp = needExp;
	}

	public int getCash() {
		return cash;
	}

	public void setCash(int cash) {
		this.cash = cash;
	}

	public Deck getDeck() {
		return deck;
	}

	public void setDeck(Deck deck) {
		this.deck = deck;
	}

	public Powers getPowers() {
		return powers;
	}

	public void setPowers(Powers powers) {
		this.powers = powers;
	}

	public Army getArmy() {
		return army;
	}

	public void setArmy(Army army) {
		this.army = army;
	}

	public Battle getBattle() {
		return battle;
	}

	public void setBattle(Battle battle) {
		this.battle = battle;
	}

	public String getBattleRole() {
		return battleRole;
	}

	public void setBattleRole(String battleRole) {
		this.battleRole = battleRole;
	}

	public Speller getSpeller() {
		return speller;
	}

	public void setSpeller(Speller speller) {
		this.speller = speller;
	}



	// Methods

	public JSONObject changeHP(int count) throws JSONException{
		JSONObject result = new JSONObject();

		count = (count < 0) ? reduceDamage(count) : count;

		result.append("target", "hero_" + this.getHeroId());
		result.append("count", count);
		result.append("newHP", currentHP += count);
		result.append("result", (currentHP <= 0) ? "dead" : "survival");

		return result;
	}

	public String getSQLUpdateDeckIdString(){
		return "UPDATE heroes SET deckId=" + this.getDeck().getDeckId() + " WHERE heroId=" + this.getHeroId();
	}

	public String getSQLUpdatePowersIdString(){
		return "UPDATE heroes SET powersId=" + this.getPowers().getPowersId() + " WHERE heroId=" + this.getHeroId();
	}

	public String getSQLUpdateBattleArmyIdString(){
		return "UPDATE heroes SET armyId=" + Util.getRandomInRange(100_000_000, 999_999_999) + " WHERE heroId=" + heroId;
	}

	public String getSQLUpdateString(){
		return
			"UPDATE heroes SET " +
				"currentHP=" + this.getCurrentHP() +
			" WHERE heroId=" + this.getHeroId();
	}

	public ArrayList<String> getSQLUpdateStrings(){

		ArrayList<String> strings = new ArrayList<>();

		strings.add(this.getSQLUpdateString());

		return strings;

	}

	public JSONObject action(Request request, Battle battle) throws IllegalActionException, JSONException, SQLException{


		long t1 = System.currentTimeMillis();

		// проверка возможности совершить данный ход в данный момент
		String actionType = request.getParameter("actionType");
		int heroId = Integer.parseInt(request.getParameter("heroId"));

		String literal = request.getParameter("literal");
		int target = Integer.parseInt(request.getParameter("target"));


		if(heroId != battle.getForwardId()) {
			throw new IllegalActionException(IllegalActionException.BROKEN_SEQUENCE);
		}

		// В случае действия с картами, дополнительные проверки
		// todo нужно убрать это блок,
		// а в качетсве замены задействовать DeckCard.isEnabledToAction()
		if(!actionType.equals("travelPass") && !actionType.equals("surrender")){

			if(!battle.getForward().getDeck().getCardsMap().containsKey(literal)) {
				throw new IllegalActionException(IllegalActionException.CARD_MISSING);
			}

			DeckCard card = battle.getForward().getDeck().getCardsMap().get(literal);

			String element = card.getElement();
			int cost = card.getCost();
			int currentPower = (int) battle.getForward().getPowers().getPowers().get(element).get("current");

			if(currentPower < cost){
				throw new IllegalActionException(IllegalActionException.INSUFFICIENT_POWER);
			}

			if(actionType.equals("setCard")){
				if(target < 0 || target > 6){
					throw new IllegalActionException(IllegalActionException.SLOT_DOES_NOT_EXIST);
				}
			}

			switch (card.getTarget()){
				case "m" : if(battle.getForward().getArmy().getCards()[target] == null){
					throw new IllegalActionException(IllegalActionException.ILLEGAL_TARGET_SPELL_MY);
				} break;
				case "e" : ArmyCard armyCard = battle.getDefender().getArmy().getCards()[target];
					if(armyCard == null || armyCard.getLiteral().equals("IceGolem")){
					throw new IllegalActionException(IllegalActionException.ILLEGAL_TARGET_SPELL_ENEMY);
				} break;
				case "i" : break;
				default: if(battle.getForward().getArmy().getCards()[target] != null){
					throw new IllegalActionException(IllegalActionException.ILLEGAL_TARGET_CARD);
				} break;
			}
		}


		JSONObject result = new JSONObject();

		//Обработка результата хода героя
		switch (actionType){
			case "setCard" : result.append("heroAction", battle.getForward().actionSetCard(literal, target)); break;
			case "castSpell" : result.append("heroAction", battle.getForward().actionCastSpell(literal, target)); break;
			case "travelPass" : result.append("heroAction", battle.getForward().actionTravelPass()); break;
			case "surrender" : result.append("heroAction", battle.getForward().actionSurrender()); break;
		}


		// Если игрок сдался или защищающийся герой погиб, продолжать бесполезно
		if(!actionType.equals("surrender")){
			if(battle.getDefender().getCurrentHP() > 0){
				result.append("armyAttack", battle.getForward().getArmy().getAttackResult());

				// Если после атаки армии, герой не погиб, ему нужно передать ход
				if(battle.getDefender().getCurrentHP() > 0){

					// Обновляем и получаем значения сил героя к которому переходит ход
					result.append("growthPowers", battle.getDefender().getPowers().updatePowersBeforeNewAction());

					// Получаем информацию о том какие карты доступны для хода
					result.append("availableToActionCards", battle.getDefender().getDeck().getAvailableToActionCards());

					// Перед переходом хода, карты героя к которому переходит ход, могут регинироваться,
					// а также наносить урон
					result.append("preAction", battle.getDefender().getArmy().preAction());
				}
			}
		}


		battle.setJustActionResult(JSONObject.quote(result.toString()));

		// Переход хода:
		// меняем местами нападающего и защищающегося, обновляем битву;
		int currentForwardId = battle.getForwardId();
		int currentDefenderId = battle.getDefenderId();
		battle.setForwardId(currentDefenderId);
		battle.setDefenderId(currentForwardId);

		long t2 = System.currentTimeMillis();
		System.out.println("action time: " + (t2 - t1));



		Factory.getInstance().battleDao.updateBattle(battle);
		long t3 = System.currentTimeMillis();
		System.out.println("update BD time: " + (t3 - t2));


		result.append("status", "success");
		return result;
	}

	public JSONObject actionSetCard(String literal, int target) throws JSONException, SQLException, IllegalActionException{


		ECards cardEnum = ECards.valueOf(literal);


		// Выводим результат вызова, снижаем силы героя.
		JSONObject heroAction = new JSONObject();
		heroAction.append("dispel", dispelPower(cardEnum.getElement(), -cardEnum.getCost()));

		int damage;
		switch (cardEnum.name()){
			case "FireElemental" : damage = -(int) powers.getPowers().get("fire").get("current"); break;
			case "WaterElemental" : damage = -(int) powers.getPowers().get("water").get("current"); break;
			case "AirElemental" : damage = -(int) powers.getPowers().get("air").get("current"); break;
			case "EarthElemental" : damage = -(int) powers.getPowers().get("earth").get("current"); break;
			default: damage = cardEnum.getDamage();
		}

		ArmyCard calledCard = new ArmyCard(cardEnum, Util.getRandomInRange(100_000_000, 999_999_999), army, target,
			damage, cardEnum.getMaxHP(), true);

		// Клонированную карту помещаем в армию
		army.getCards()[target] = calledCard;
		calledCard = army.getCards()[target];

		JSONObject card = new JSONObject();
		JSONObject whenCalling = new JSONObject();

		card.append("cardId", calledCard.getCardId());
		card.append("cost", calledCard.getCost());
		card.append("damage", calledCard.getPositiveDamage());
		card.append("hp", calledCard.getCurrentHP());
		card.append("photo", calledCard.getPhoto());
		card.append("description", calledCard.getDescription());

		heroAction.append("actionType", "setCard");
		heroAction.append("card", card);
		heroAction.append("literal", literal);
		heroAction.append("heroId", "hero_" + heroId);
		heroAction.append("armyId", army.getArmyId());
		heroAction.append("target", target);
		whenCalling.append("increaseDamage", calledCard.increaseDamage("call"));
		whenCalling.append("changeHP", calledCard.changeHPWhenCalling());
		whenCalling.append("changePowers", calledCard.changePowers("calling"));
		whenCalling.append("additionalSummon", calledCard.additionalSummon());
		heroAction.append("whenCalling", whenCalling);
		heroAction.append("actionResult",
			(battle.getDefender().getCurrentHP() > 0)
			? "continues"
			: battle.finalizeBattle("defenderDead")
		);

		return heroAction;
	}

	public JSONObject actionCastSpell(String literal, int target) throws JSONException, SQLException, IllegalActionException{

		ECards spell = ECards.valueOf(literal);
		String element = spell.getElement();
		int cost = spell.getCost();

		JSONObject heroAction = new JSONObject();
		heroAction.append("actionType", "castSpell");
		heroAction.append("heroId", "hero_" + battle.getForward().getHeroId());
		heroAction.append("literal", literal);
		heroAction.append("spellResult", speller.apply(literal, target));
		heroAction.append("dispel", dispelPower(element, -cost));
		heroAction.append("actionResult", (battle.getDefender().getCurrentHP() > 0) ? "continues" : battle.finalizeBattle("defenderDead"));

		return heroAction;
	}

	public JSONObject actionTravelPass() throws JSONException {
		JSONObject heroAction = new JSONObject();
		heroAction.append("actionType", "travelPass");
		heroAction.append("heroId", "hero_" + battle.getForward().getHeroId());
		heroAction.append("actionResult", "continues");
		return heroAction;
	}

	public JSONObject actionSurrender() throws JSONException, SQLException {
		JSONObject heroAction = new JSONObject();
		heroAction.append("actionType", "surrender");
		heroAction.append("heroId", "hero_" + this.getHeroId());
		heroAction.append("actionResult", this.getBattle().finalizeBattle("surrender"));
		return heroAction;
	}

	public JSONObject dispelPower(String element, int count) throws JSONException{

		JSONObject dispel = new JSONObject();
		dispel.append("powersId", powers.getPowersId());
		dispel.append("powers", new JSONObject().append(element, powers.changePower(element, "current", count)));

		return dispel;
	}

	public boolean isForward(){
		return (heroId == battle.getForwardId());
	}

	public int reduceDamage(int count){

		double percentage = 100;

		for (ArmyCard card : army.getCards()){
			if(card != null && card.getLiteral().equals("IceGuard")){
				percentage /= 2;
			}
		}

		return (int) Math.ceil(Math.abs(count) * percentage/100) * -1;
	}
}
